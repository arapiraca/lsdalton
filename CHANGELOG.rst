=========
ChangeLog
=========

Unreleased
----------

Added
+++++

- Python bindings generated using `CFFI <https://cffi.readthedocs.io>`_ in API-mode

Breaking
++++++++

- Minimum required Python version is 3.6
- Minimum required CMake version is 3.14

Changed
+++++++

- Update PCMSolver to its 1.2.3 version
- Derivative integrals/integral components for general-order electrostatic potentials
- Minor improvements for geometry optimization: retain original atomic ordering and print xyz file
- Source files are specified per directory, linked into ``OBJECT`` libraries,
  and finally into a single ``lsdalton`` library
- Tests are declared to CTest per directory. The macros for declaring tests have
  changed. They are still variadic, but they now accept named parameters
- Added ``environment.yml`` file to manage development environment with Conda
- ``lsdaltonpy`` tests now have reference files in ``.npy`` format. Closes #63
- Update XCFun to its 2.0.0a1 version. Note that XCFun is not cloned anymore as
  a submodule of LSDALTON, but rather downloaded at configuration if need be.
  This change allows you to use pre-installed versions of the library: if XCFun
  was installed in ``<xcfun-prefix>`` then it can be used by LSDALTON passing
  this flag to the ``setup`` script:
  ``-DXCFun_DIR=<xcfun-prefix>/share/cmake/XCFun``
- The structure of the install tree has been changed to conform to the GNU
  standard. Assuming you install into the folder ``<install-prefix>``, the
  directory structure will be as follows::

    <install-prefix>/
                    ├── bin
                    │   ├── lsdalton
                    │   ├── lsdalton.x
                    │   └── lslib_tester.x
                    ├── lib
                    │   └── python
                    │       ├── conftest.py
                    │       ├── data
                    │       ├── pylsdalton
                    │       ├── test_integrals.py
                    │       └── test_scf.py
                    ├── lib64
                    │   └── liblsdalton.a
                    └── share
                        └── lsdalton
                            ├── basis
                            └── tools
- External submodules are fetched during configuration, if requested, rather
  than being bundled as Git submodules.

LSDalton release v2018.0 (2018-11-19)
-------------------------------------

Added
+++++

- General-order electric and geometrical derivatives through a combination
  of the OpenRSP library and HODI integral routines (alpha version, HF only)
- Tests on PCM static and dynamic first hyperpolarizability
- Tests on PCM Two Photon Absorption
- Tests on PCM static and dynamic second hyperpolarizability (only HF)
- SVD solver option for df-J
- ADMM basis sets and electrical response properties
- Multilayer DEC (ML-DEC) scheme for combinations of HF, RI-MP2, CCSD, and CCSD(T)

Changed
+++++++

- Docker images for CI. The image now used is based on Ubuntu 16.04 and
  includes GCC 5.3.1, OpenMPI 1.10 and MKL 2017.4.239.
  The Dockerfile can be found `here <https://github.com/robertodr/dockerfiles/blob/master/Dockerfile.ubuntu-16.04_gcc5.3.1_openmpi1.10_mkl2017.4.239>`_.

Fixed
+++++

- ``setup`` script, test script, compilations framework, and more, compatible with Python 3.
- fix for ADMM in combination with ScaLapack

1.2 (2016-02-08)
----------------

- A bug in using cartesian basis functions was identified and fixed.

1.1 (2016-01-13)
----------------

- The "make install" was fixed.

1.0 (2015-12-22)
----------------

See the release notes at http://daltonprogram.org for a list of new features in
Dalton and LSDalton.
