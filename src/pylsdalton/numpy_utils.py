import numpy as np
from ._pylsdalton import ffi, lib

def numpy_mat1_routine(routine, geoOrder, magOrder) -> np.ndarray:
    """Wrapper routine to create, calculate and return a single numpy AO matrix
    Args: 
        routine: The routine that calculates the AO matrix (takes the matrix and its dimension as input)
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order
 
    Returns:
        differentiated/undifferentiated AO matrices as specified by the input routine
    """
    N = lib.pylsd_nbasis()
    nDeriv = numpy_nderiv(geoOrder, magOrder)
    
    if nDeriv == 1:
        mat = np.zeros(shape=(N, N), dtype=np.float64)
    else:
        mat = np.zeros(shape=(nDeriv, N, N), dtype=np.float64)
    pmat = ffi.from_buffer("double*", mat, require_writable=True)
    routine(pmat, N, nDeriv, geoOrder, magOrder)
    return mat


def numpy_matn_routine(routine, Dmat, geoOrder, magOrder, *args) -> np.ndarray:
    """Wrapper routine to create, calculate and return density-contracted AO matrices
    Args: 
        routine: The routine that calculates the density-contracted AO matrix
        dmat: The AO density matrix
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order
        *args: additional argments to be passed
 
    Returns:
        differentiated/undifferentiated AO matrices as specified by the input routine
    """
    N = lib.pylsd_nbasis()
    nmat = 1
    if len(Dmat.shape) == 3:
        nmat = Dmat.shape[0]
    pDmat = ffi.from_buffer("double*", Dmat)

    nDeriv = numpy_nderiv(geoOrder, magOrder)

    if nDeriv == 1 and nmat == 1:
        mat = np.zeros(shape=(N, N), dtype=np.float64)
    elif nDeriv == 1:
        mat = np.zeros(shape=(nmat, N, N), dtype=np.float64)
    elif nmat == 1:
        mat = np.zeros(shape=(nDeriv, N, N), dtype=np.float64)
    else:
        mat = np.zeros(shape=(nmat, nDeriv, N, N), dtype=np.float64)

    pmat = ffi.from_buffer("double*", mat, require_writable=True)
    routine(pmat, pDmat, N, nmat, nDeriv, geoOrder, magOrder, *args)
    return mat

def numpy_nderiv(geoOrder, magOrder):
    """Routine to calculate the number of derivative components
    Args: 
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        combined number of magnetic and geometrical derivative components
    """
    nAtoms = lib.pylsd_natoms()

    nMag = binom(3 + magOrder - 1, magOrder)
    nGeo = binom(3*nAtoms + geoOrder - 1, geoOrder) 

    return nGeo*nMag

def binom(n: int,k: int):
    """Calculates the binomial coefficient
    Args: 
        n: main integer
        k: secondary integer

    Returns:
        the binomial coefficient n!/(k!(n-k)!)
    """
    if k < 0:
        result = 0
    else:
        large: int=1
        small: int=1
        for i in range(min(k,n-k)):
            large = large*(n-i)
            small = small*(i+1)
        result = large/small
    return int(result)

def numpy_XC_matrix(routine, Dmat, geoOrder, magOrder, testElectrons):
    """Wrapper routine to create, calculate and return XC energies and matrices
    Args: 
        routine: The routine that calculates the density-contracted XC matrix
        dmat: The AO density matrix
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order
        testElectrons: Test if the density integrates to the number of electrons
 
    Returns:
        XC energies and AO matrices
    """
    N = lib.pylsd_nbasis()
    nmat = 1
    if len(Dmat.shape) == 3:
        nmat = Dmat.shape[0]
    pDmat = ffi.from_buffer("double*", Dmat)

    nDeriv = numpy_nderiv(geoOrder, magOrder)

    mat = np.zeros(shape=(nmat * nDeriv, N, N), dtype=np.float64)
    pmat = ffi.from_buffer("double*", mat, require_writable=True)
    XC_energy = np.zeros(shape=nmat, dtype=np.float64)
    pXC = ffi.from_buffer("double*", XC_energy, require_writable=True)
    routine(pmat, pDmat, N, nmat, nDeriv, geoOrder, magOrder, pXC, testElectrons)
    return XC_energy, mat

def numpy_ep_matrix(moments, points, epOrder, geoOrder, magOrder):
    """Wrapper routine to create, calculate and return the electronic electrostatic potential

    Args: 
        moments: Multipole moments (shape=(nEPcomp,nPoints))
        points: points (shape=(nPoints,3)) where the electrostatic potential is to be evaluated
        epOrder: range of multipole moment order - 0 charge, 1 dipole, 2 quadropoles, etc.
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        Point-wise electronic multipol-moment potential
    """
    if epOrder[0] < 0:
        raise AttributeError('Error in numpy_ep_matrix. Start ep-order < 0')
    if epOrder[0] > epOrder[1]:
        raise AttributeError('Error in numpy_ep_matrix. Start ep order > end ep order')

    N = lib.pylsd_nbasis()
    pmoments = ffi.from_buffer("double*", moments)

    nPoints = points.shape[0]
    ppoints = ffi.from_buffer("double*", points)
    nEPcomp = binom(3 + epOrder[1], epOrder[1]) - binom(3 + epOrder[0], epOrder[0] - 1)

    if moments.ndim == 2:
        if nPoints != moments.shape[1]:
            raise AttributeError('Number of moments mismatch in numpy_ep_potential')
        if nEPcomp != moments.shape[0]:
            raise AttributeError('Number of EP components mismatch in numpy_ep_potential')
    else:
        if nPoints != moments.shape[0]:
            raise AttributeError('Number of moments mismatch in numpy_ep_potential')

    nDeriv = numpy_nderiv(geoOrder, magOrder)

    pot = np.zeros(shape=(nDeriv, N, N), dtype=np.float64)
    epmat = ffi.from_buffer("double*", pot, require_writable=True)

    lib.pylsd_electrostatic_potential_matrix(epmat, pmoments, N, ppoints, nPoints, nEPcomp, 
                                             epOrder[0], epOrder[1], nDeriv, geoOrder, magOrder)
    if nDeriv > 1:
        return pot
    else:
        return np.reshape(pot, (N,N))

def numpy_ep_vector(Dmat, points, epOrder, geoOrder, magOrder):
    """Wrapper routine to create, calculate and return the electronic electrostatic potential

    Args: 
        Dmat: The AO density matrices.
        points: points(nP,3) where the electrostatic potential is to be evaluated
        epOrder: range of multipole moment order - 0 charge, 1 dipole, 2 quadropoles, etc.
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        Point-wise electronic multipol-moment potential
    """
    if epOrder[0] < 0:
        raise AttributeError('Error in numpy_ep_vector. Start ep-order < 0')
    if epOrder[0] > epOrder[1]:
        raise AttributeError('Error in numpy_ep_vector. Start ep order > end ep order')

    N = lib.pylsd_nbasis()
    nmat = 1
    if len(Dmat.shape) == 3:
        nmat = Dmat.shape[0]
    pDmat = ffi.from_buffer("double*", Dmat)

    if len(points.shape) == 2:
        nPoints = points.shape[0]
    else:
        nPoints = 1

    ppoints = ffi.from_buffer("double*", points)
    nEPcomp = binom(3 + epOrder[1], epOrder[1]) - binom(3 + epOrder[0], epOrder[0] - 1)

    nDeriv = numpy_nderiv(geoOrder, magOrder)

    pot = np.zeros(shape=(nmat, nEPcomp, nDeriv, nPoints), dtype=np.float64)
    eppot = ffi.from_buffer("double*", pot, require_writable=True)

    lib.pylsd_electrostatic_potential_vector(eppot, pDmat, N, nmat, ppoints, nPoints, nEPcomp, 
                                             epOrder[0], epOrder[1], nDeriv, geoOrder, magOrder)
    if nmat > 1 and nDeriv > 1 and nEPcomp > 1:
        return pot
    elif nmat > 1 and nDeriv > 1:
        return np.reshape(pot, (nmat, nDeriv, nPoints))
    elif nmat > 1 and nEPcomp > 1:
        return np.reshape(pot, (nmat, nEPcomp, nPoints))
    elif nDeriv > 1 and nEPcomp > 1:
        return np.reshape(pot, (nEPcomp, nDeriv, nPoints))
    elif nEPcomp > 1:
        return np.reshape(pot, (nEPcomp, nPoints))
    elif nmat > 1: 
        return np.reshape(pot, (nmat, nPoints))
    elif nDeriv > 1: 
        return np.reshape(pot, (nDeriv, nPoints))
    else:
        return np.reshape(pot, (nPoints))
        

def numpy_ep_integrals(points, epOrder, geoOrder, magOrder):
    """Wrapper routine to create, calculate and return the electrostatic potential integrals

    Args: 
        points: points(nP,3) where the electrostatic potential is to be evaluated
        epOrder: range of multipole moment order - 0 charge, 1 dipole, 2 quadropoles, etc.
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        Point-wise electronic multipol-moment integrals
    """
    if epOrder[0] < 0:
        raise AttributeError('Error in numpy_ep_integrals. Start ep-order < 0')
    if epOrder[0] > epOrder[1]:
        raise AttributeError('Error in numpy_ep_integrals. Start ep order > end ep order')

    N = lib.pylsd_nbasis()

    nPoints = points.shape[0]
    ppoints = ffi.from_buffer("double*", points)
    nEPcomp = binom(3 + epOrder[1], epOrder[1]) - binom(3 + epOrder[0], epOrder[0] - 1)

    nDeriv = numpy_nderiv(geoOrder, magOrder)

    pot = np.zeros(shape=(nEPcomp, nDeriv, nPoints, N, N), dtype=np.float64)
    eppot = ffi.from_buffer("double*", pot, require_writable=True)

    lib.pylsd_electrostatic_potential_integrals(eppot, N, ppoints, nPoints, nEPcomp, 
                                                epOrder[0], epOrder[1], nDeriv, geoOrder, magOrder)
    if nDeriv > 1 and nEPcomp > 1:
        return pot
    elif nDeriv > 1:
        return np.reshape(pot, (nDeriv, nPoints, N, N))
    elif nEPcomp > 1:
        return np.reshape(pot, (nEPcomp, nPoints, N, N))
    else:
        return np.reshape(pot, (nPoints, N, N))



def numpy_diagonalize(routine, Hmat, Smat):
    """Wrapper routine to create, calculate and return molecular orbitals and energies
    Args: 
        routine: The routine that diagonalize the Hamiltonian 
        Hmat: The AO Hamiltonian matrix
        Smat: The AO overlap matrix
 
    Returns:
        energies, MOs: molecular orbital energies and coefficients
    """
    N = lib.pylsd_nbasis()
    pH = ffi.from_buffer("double*", Hmat)
    pS = ffi.from_buffer("double*", Smat)
    MOs = np.zeros(shape=(N, N), dtype=np.float64)
    pMOs = ffi.from_buffer("double*", MOs, require_writable=True)
    energies = np.zeros(shape=(N), dtype=np.float64)
    pE = ffi.from_buffer("double*", energies, require_writable=True)
    routine(pMOs, pE, pH, pS, N)
    return energies, MOs


def numpy_diagonal_density(routine, Hmat, Smat):
    """Wrapper routine to create, calculate and return AO density matrix
    Args: 
        routine: The routine that diagonalize the Hamiltonian 
        Hmat: The AO Hamiltonian matrix
        Smat: The AO overlap matrix
 
    Returns:
        Dmat: The AO denisty matrix obtained by diagonalization
    """
    N = lib.pylsd_nbasis()
    pH = ffi.from_buffer("double*", Hmat)
    pS = ffi.from_buffer("double*", Smat)
    D = np.zeros(shape=(N, N), dtype=np.float64)
    pD = ffi.from_buffer("double*", D, require_writable=True)
    routine(pD, pH, pS, N)
    return D

def lspy_test_derivative_order(function_name, geoOrder, magOrder, geoMax: int=0, magMax: int=0, mixedGeoMag: bool=False):
    """Routine to set and test maximum geometrical and magnetic derivative orders
    Args: 
        function_name: Name of the calling routine
        geoOrder: requested geometrical derivative order
        magOrder: requested magnetic derivative order
        geoMax: maximal geometrical derivative order
        magMax: maximal magnetic derivative order
        mixedGeoMag: specifies if mixed geometrical and magnetic derivatives are allowed
 
    Notes:
        raise excetion if maximal derivative orders are exceeded
    """
    if geoOrder > geoMax:
        raise AttributeError(f'Geometrical derivative order exceeded in {function_name}. geoOrder > geoMax: {geoOrder} > {geoMax}')
    if magOrder > magMax:
        raise AttributeError(f'Magnetic derivative order exceeded in {function_name}. magOrder > magMax: {magOrder} > {magMax}')
    if geoOrder > 0 and magOrder > 0 and not mixedGeoMag:
        raise AttributeError(f'Mixed magnetic and geometrical derivatived not allowed for {function_name}. geoOrder, magOrder: {geoOrder}, {magOrder}')

