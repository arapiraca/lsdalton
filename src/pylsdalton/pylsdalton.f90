module pylsdalton
   use, intrinsic :: iso_c_binding
   use lslib_state
   use lslib
   use precision
   use ls_string

   implicit none

   private

   interface
      ! This function is provided by the Python/C interface layer
      subroutine logger(message, level) bind(C)
        import
        character(kind=c_char, len=1), intent(in) :: message(*)
        integer(c_int), intent(in), value :: level
      end subroutine logger
   end interface

   public pylsd_create, pylsd_destroy, pylsd_natoms, pylsd_nbasis, pylsd_nelectrons, &
     &    pylsd_overlap_matrix, pylsd_kinetic_energy_matrix, &
     &    pylsd_nuclear_electron_attraction_matrix, pylsd_coulomb_matrix, &
     &    pylsd_exchange_matrix, pylsd_exchange_correlation_matrix, &
     &    pylsd_fock_matrix, pylsd_diagonalize, pylsd_diagonal_density, pylsd_eri, &
     &    pylsd_electrostatic_potential_matrix, pylsd_electrostatic_potential_vector, &
     &    pylsd_electrostatic_potential_integrals

contains

  subroutine pylsd_logger(message, level)
  use, intrinsic :: iso_c_binding, only: c_int
  use ls_string, only: fstring_to_carray
  implicit none
  character(len=*), intent(in) :: message
  integer, intent(in) :: level

  call logger(fstring_to_carray(message), int(level, kind=c_int))
  end subroutine pylsd_logger

  !> \brief Creates an lsdalton instance by reading the LSDALTON.INP and MOLECULE.INP
  !> \author S. Reine
  !> \date 2019-11-13
  subroutine pylsd_create(dalfile, len_dalfile, molfile, len_molfile) bind(c)
  use logging, only: logger_init
  integer(c_int), value, intent(in) :: len_dalfile, len_molfile
  character(kind=c_char, len=1), intent(in) :: dalfile(len_dalfile), molfile(len_molfile)
  integer :: lupri, luerr
  logical :: OnMaster

  OnMaster       = .TRUE.
  luerr = LSlibconfig%luerr
  lupri = LSlibconfig%lupri

  call logger_init(pylsd_logger)

  call LSlib_init(OnMaster,lupri,luerr, &
       & carray_to_fstring(dalfile, len_dalfile), &
       & carray_to_fstring(molfile, len_molfile))

  end subroutine pylsd_create

   !> \brief Destroys the lsdalton instance
   !> \author S. Reine
   !> \date 2019-11-13
   subroutine pylsd_destroy() bind(c)
   integer :: lupri, luerr
   logical :: OnMaster

   OnMaster       = .TRUE.
   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri
   call LSlib_free(OnMaster,lupri,luerr, .FALSE.)

   end subroutine pylsd_destroy

   !> \brief Return the number of atoms (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-13
   function pylsd_natoms() result(natoms) bind(c)
   integer(c_int) :: natoms
   integer :: nbast,natoms_,nelectrons

   call LSlib_get_dimensions(nbast,natoms_,nelectrons,LSlibconfig%lupri,LSlibconfig%luerr)
   natoms = int(natoms_, kind=c_int)

   end function pylsd_natoms

   !> \brief Return the number of AO basis functions (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-13
   function pylsd_nbasis() result(nbast) bind(c)
   integer(c_int) :: nbast
   integer :: natoms,nelectrons,nbast_

   call LSlib_get_dimensions(nbast_,natoms,nelectrons,LSlibconfig%lupri,LSlibconfig%luerr)
   nbast = int(nbast_, kind=c_int)

   end function pylsd_nbasis

   !> \brief Return the number of atoms (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-13
   function pylsd_nauxbasis() result(nbast) bind(c)
   integer(c_int) :: nbast
   integer :: natoms,nelectrons

   nbast = int(LSlib_get_nAux(LSlibconfig%lupri,LSlibconfig%luerr), kind=c_int)

   end function pylsd_nauxbasis

   !> \brief Return the number of electrons (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-13
   function pylsd_nelectrons() result(nelectrons) bind(c)
   integer(c_int) :: nelectrons
   integer :: nbast,natoms,nelectrons_

   call LSlib_get_dimensions(nbast,natoms,nelectrons_,LSlibconfig%lupri,LSlibconfig%luerr)
   nelectrons = int(nelectrons_, kind=c_int)

   end function pylsd_nelectrons

   !> \brief Calculates the overlap matrix (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-13
   !> \param S The overlap matrix (allocated outside)
   !> \param nbasis The number of AO basis functions
   !> \param nderiv The total number of derivative components (geometric and magnetic)
   !> \param geoOrder The geometrical derivative order
   !> \param magOrder The magnetic derivative order
   subroutine pylsd_overlap_matrix(S, nbasis, nDeriv, geoOrder, magOrder) bind(c)
   integer(c_int), intent(in), value :: nbasis, nDeriv, geoOrder, magOrder
   real(c_double), intent(inout) :: S(nbasis,nbasis)

   integer :: lupri, luerr, nbasis_, nDeriv_, geoOrder_, magOrder_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri
   nbasis_ = int(nbasis, kind=intk)
   nDeriv_ = int(nDeriv, kind=intk)
   geoOrder_ = int(geoOrder, kind=intk)
   magOrder_ = int(magOrder, kind=intk)
   call LSlib_get_overlap(S,nbasis_,nDeriv_,geoOrder_,magOrder_,lupri,luerr)
   end subroutine pylsd_overlap_matrix

   !> \brief Calculates the kinetic-energy matrix (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-14
   !> \param k The kinetic-energy matrix (allocated outside)
   !> \param nbasis The number of AO basis functions
   !> \param nderiv The total number of derivative components (geometric and magnetic)
   !> \param geoOrder The geometrical derivative order
   !> \param magOrder The magnetic derivative order
   subroutine pylsd_kinetic_energy_matrix(k, nbasis, nDeriv, geoOrder, magOrder) bind(c)
   integer(c_int), intent(in), value :: nbasis, nDeriv, geoOrder, magOrder
   real(c_double), intent(inout) :: k(nbasis,nbasis)

   integer :: lupri, luerr, nbasis_, nDeriv_, geoOrder_, magOrder_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri
   nbasis_ = int(nbasis, kind=intk)
   nDeriv_ = int(nDeriv, kind=intk)
   geoOrder_ = int(geoOrder, kind=intk)
   magOrder_ = int(magOrder, kind=intk)
   call LSlib_get_kinetic_mat(k,nbasis_,nDeriv_,geoOrder_,magOrder_,lupri,luerr)
   end subroutine pylsd_kinetic_energy_matrix

   !> \brief Calculates the nuclear-electron-attraction matrix (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-14
   !> \param h The nuclear-electron-attraction matrix (allocated outside)
   !> \param nbasis The number of AO basis functions
   !> \param nderiv The total number of derivative components (geometric and magnetic)
   !> \param geoOrder The geometrical derivative order
   !> \param magOrder The magnetic derivative order
   subroutine pylsd_nuclear_electron_attraction_matrix(h, nbasis, nDeriv, geoOrder, magOrder) bind(c)
   integer(c_int), intent(in), value :: nbasis, nDeriv, geoOrder, magOrder
   real(c_double), intent(inout) :: h(nbasis,nbasis)
   !
   integer :: lupri, luerr, nbasis_, nDeriv_, geoOrder_, magOrder_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri
   nbasis_ = int(nbasis, kind=intk)
   nDeriv_ = int(nDeriv, kind=intk)
   geoOrder_ = int(geoOrder, kind=intk)
   magOrder_ = int(magOrder, kind=intk)
   call LSlib_get_nucel_mat(h,nbasis_,nDeriv_,geoOrder_,magOrder_,lupri,luerr)
   end subroutine pylsd_nuclear_electron_attraction_matrix

   !> \brief Calculates the Coulumb AO matrix (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-14
   !> \param J The AO Coulomb matrices (allocated outside)
   !> \param D The AO density matrices (allocated outside)
   !> \param nbasis The number of AO basis functions
   !> \param ndmat The number of AO density matrices functions
   !> \param nderiv The total number of derivative components (geometric and magnetic)
   !> \param geoOrder The geometrical derivative order
   !> \param magOrder The magnetic derivative order
   subroutine pylsd_coulomb_matrix(J, D, nbasis, ndmat, nDeriv, geoOrder, magOrder) bind(c)
   integer(c_int), intent(in), value :: nbasis, nDeriv, geoOrder, magOrder
   integer(c_int), intent(in), value :: ndmat
   real(c_double), intent(inout) :: J(nbasis,nbasis,ndmat)
   real(c_double), intent(in) :: D(nbasis,nbasis,ndmat)
   !
   integer :: lupri, luerr, nbasis_, ndmat_, nDeriv_, geoOrder_, magOrder_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri
   nbasis_ = int(nbasis, kind=intk)
   ndmat_ = int(ndmat, kind=intk)
   nDeriv_ = int(nDeriv, kind=intk)
   geoOrder_ = int(geoOrder, kind=intk)
   magOrder_ = int(magOrder, kind=intk)
   call LSlib_get_Coulomb(J,D,nbasis_,ndmat_,nDeriv_,geoOrder_,magOrder_,lupri,luerr)
   end subroutine pylsd_coulomb_matrix

   !> \brief Calculates the exchange AO matrix (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-14
   !> \param K The AO exchange matrices (allocated outside)
   !> \param D The AO density matrices (allocated outside)
   !> \param nbasis The number of AO basis functions
   !> \param ndmat The number of AO density matrices functions
   !> \param dsym True if all density matrices are symmetric
   !> \param testElectrons Test whether electron density integrates to the number of electrons
   !> \param nderiv The total number of derivative components (geometric and magnetic)
   !> \param geoOrder The geometrical derivative order
   !> \param magOrder The magnetic derivative order
   subroutine pylsd_exchange_matrix(K, D, nbasis, ndmat, nDeriv, geoOrder, magOrder, dsym, testElectrons) bind(c)
   integer(c_int), intent(in), value :: nbasis, nDeriv, geoOrder, magOrder
   integer(c_int), intent(in), value :: ndmat
   real(c_double), intent(inout) :: K(nbasis,nbasis,ndmat)
   real(c_double), intent(in) :: D(nbasis,nbasis,ndmat)
   logical(c_bool), intent(in), value :: dsym
   logical(c_bool), intent(in), value :: testElectrons
   !
   integer :: lupri, luerr, nbasis_, ndmat_, nDeriv_, geoOrder_, magOrder_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri
   nbasis_ = int(nbasis, kind=intk)
   ndmat_ = int(ndmat, kind=intk)
   nDeriv_ = int(nDeriv, kind=intk)
   geoOrder_ = int(geoOrder, kind=intk)
   magOrder_ = int(magOrder, kind=intk)
   call LSlib_get_exchange(K,D,nbasis_,ndmat_,logical(dsym),nDeriv_,geoOrder_,magOrder_,lupri,luerr,logical(testElectrons))
   end subroutine pylsd_exchange_matrix

   !> \brief Calculates the XC AO matrix (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-14
   !> \param X The AO exchange-correlation matrices (allocated outside)
   !> \param D The AO density matrices (allocated outside)
   !> \param EXC The exchange-correlation energy
   !> \param nbasis The number of AO basis functions
   !> \param ndmat The number of AO density matrices functions
   !> \param test_electrons Test whether electron density integrates to the number of electrons
   !> \param nderiv The total number of derivative components (geometric and magnetic)
   !> \param geoOrder The geometrical derivative order
   !> \param magOrder The magnetic derivative order
   !> ToDo: how do we deal with unrestricted case
   subroutine pylsd_exchange_correlation_matrix(X, D, nbasis, ndmat, nDeriv, geoOrder, magOrder, EXC, testElectrons) bind(c)
   integer(c_int), intent(in), value :: nbasis, nDeriv, geoOrder, magOrder
   integer(c_int), intent(in), value :: ndmat
   real(c_double), intent(inout)     :: EXC(ndmat)
   real(c_double), intent(inout)     :: X(nbasis,nbasis,ndmat)
   real(c_double), intent(in)        :: D(nbasis,nbasis,ndmat)
   logical(c_bool), intent(in), value :: testElectrons
   !
   integer                    :: lupri, luerr, nbasis_, ndmat_, nDeriv_, geoOrder_, magOrder_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri
   nbasis_ = int(nbasis, kind=intk)
   ndmat_ = int(ndmat, kind=intk)
   nDeriv_ = int(nDeriv, kind=intk)
   geoOrder_ = int(geoOrder, kind=intk)
   magOrder_ = int(magOrder, kind=intk)
   call LSlib_get_XC(X,D,EXC,nbasis_,ndmat_,nDeriv_,geoOrder_,magOrder_,lupri,luerr,logical(testElectrons))
   end subroutine pylsd_exchange_correlation_matrix

   !> \brief Calculates the AO Fock matrix (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-14
   !> \param F The AO Fock matrices (allocated outside)
   !> \param D The AO density matrices (allocated outside)
   !> \param nbasis The number of AO basis functions
   !> \param ndmat The number of AO density matrices functions
   !> \param test_electrons Test whether electron density integrates to the number of electrons
   !> \param nderiv The total number of derivative components (geometric and magnetic)
   !> \param geoOrder The geometrical derivative order
   !> \param magOrder The magnetic derivative order
   subroutine pylsd_fock_matrix(F, D, nbasis, ndmat, nDeriv, geoOrder, magOrder, testElectrons) bind(c)
   integer(c_int), intent(in), value :: nbasis, nDeriv, geoOrder, magOrder
   integer(c_int), intent(in), value :: ndmat
   real(c_double), intent(inout) :: F(nbasis,nbasis,ndmat)
   real(c_double), intent(in) :: D(nbasis,nbasis,ndmat)
   logical(c_bool), intent(in), value :: testElectrons
   !
   integer                    :: lupri, luerr, nbasis_, ndmat_, nDeriv_, geoOrder_, magOrder_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri

   nbasis_ = int(nbasis, kind=intk)
   ndmat_ = int(ndmat, kind=intk)
   nDeriv_ = int(nDeriv, kind=intk)
   geoOrder_ = int(geoOrder, kind=intk)
   magOrder_ = int(magOrder, kind=intk)
   call LSlib_get_fock(F,D,nbasis_,ndmat_,nDeriv_,geoOrder_,magOrder_,.TRUE.,lupri,luerr,logical(testElectrons))

   end subroutine pylsd_fock_matrix

   !> \brief Calculates the AO electrostatic potential matrix
   !> \author S. Reine
   !> \date 2020-05-20
   !> \param ep_mat Electrostatic-potential matrix
   !> \param moments Multipole moments
   !> \param nbasis The number of AO basis functions
   !> \param points Multipole expansion points
   !> \param nPoints The number of points
   !> \param nEPcomp The number of electrostatic-potential components
   !> \param startEP The start order of the EP moments - 0 charge, 1 dipole, etc
   !> \param endEP The end order of the EP moments - 0 charge, 1 dipole, etc
   !> \param nderiv The total number of derivative components (geometric and magnetic)
   !> \param geoOrder The geometrical derivative order
   !> \param magOrder The magnetic derivative order
   !> Calculates Coulomb interaction between AO products and an expansion of point 
   !> multipole moments, where (startEP, endEP) sets the range of multipole-moment 
   !> orders --- endEP must be greater than or equal to startEP
   subroutine pylsd_electrostatic_potential_matrix(ep_mat, moments, nbasis, points, nPoints, &
     &           nEPcomp, startEP, endEP, nDeriv, geoOrder, magOrder) bind(c)
   implicit none
   integer(c_int), intent(in), value :: nbasis, nPoints, nEPcomp, startEP, endEP, nDeriv, geoOrder, magOrder
   real(c_double), intent(inout) :: ep_mat(nbasis,nbasis)
   real(c_double), intent(in) :: moments(nPoints, nEPcomp)
   real(c_double), intent(in) :: points(3, nPoints)
   !
   integer :: lupri, luerr, nbasis_, nPoints_, nEPcomp_, startEP_, endEP_, nDeriv_, geoOrder_, magOrder_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri

   nbasis_ = int(nbasis, kind=intk)
   nPoints_ = int(nPoints, kind=intk)
   nEPcomp_ = int(nEPcomp, kind=intk)
   startEP_ = int(startEP, kind=intk)
   endEP_ = int(endEP, kind=intk)
   nDeriv_ = int(nDeriv, kind=intk)
   geoOrder_ = int(geoOrder, kind=intk)
   magOrder_ = int(magOrder, kind=intk)
   call LSlib_get_ep_matrix(ep_mat, moments, nbasis_, points, nPoints_, nEPcomp_, startEP_, endEP_, &
     &                      nDeriv_, geoOrder_, magOrder_, lupri, luerr)

   end subroutine pylsd_electrostatic_potential_matrix

   !> \brief Calculates the electronic electrostatic-potential vector
   !> \author S. Reine
   !> \date 2020-05-20
   !> \param ep_vec Electrostatic-potential vector
   !> \param Dmat AO density matrix
   !> \param nbasis The number of AO basis functions
   !> \param ndmat The number AO density matrices
   !> \param points Multipole expansion points
   !> \param nPoints The number of points
   !> \param nEPcomp The number of electrostatic-potential components
   !> \param startEP The start order of the EP moments - 0 charge, 1 dipole, etc
   !> \param endEP The end order of the EP moments - 0 charge, 1 dipole, etc
   !> \param nderiv The total number of derivative components (geometric and magnetic)
   !> \param geoOrder The geometrical derivative order
   !> \param magOrder The magnetic derivative order
   !> Calculates the Coulomb interaction between the electron density and point 
   !> multipole moments, where (startEP, endEP) sets the range of multipole-moment 
   !> orders --- endEP must be greater than or equal to startEP
   subroutine pylsd_electrostatic_potential_vector(ep_vec, Dmat, nbasis, ndmat, points, nPoints, &
     &           nEPcomp, startEP, endEP, nDeriv, geoOrder, magOrder) bind(c)
   implicit none
   integer(c_int), intent(in), value :: nbasis, ndmat, nPoints, nEPcomp, startEP, endEP, nDeriv, geoOrder, magOrder
   real(c_double), intent(inout) :: ep_vec(nPoints, nDeriv, nEPcomp, ndmat)
   real(c_double), intent(in) :: Dmat(nbasis, nbasis, ndmat)
   real(c_double), intent(in) :: points(3, nPoints)
   !
   integer :: lupri, luerr, nbasis_, nPoints_, nEPcomp_, startEP_, endEP_, nDeriv_, geoOrder_, magOrder_, ndmat_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri

   nbasis_ = int(nbasis, kind=intk)
   ndmat_ = int(ndmat, kind=intk)
   nPoints_ = int(nPoints, kind=intk)
   nEPcomp_ = int(nEPcomp, kind=intk)
   startEP_ = int(startEP, kind=intk)
   endEP_ = int(endEP, kind=intk)
   nDeriv_ = int(nDeriv, kind=intk)
   geoOrder_ = int(geoOrder, kind=intk)
   magOrder_ = int(magOrder, kind=intk)
   call LSlib_get_ep_vector(ep_vec, Dmat, nbasis_, ndmat_, points, nPoints_, nEPcomp_, startEP_, endEP_, &
     &                      nDeriv_, geoOrder_, magOrder_, lupri, luerr)

   end subroutine pylsd_electrostatic_potential_vector

   !> \brief Calculates the AO electrostatic potential integrals
   !> \author S. Reine
   !> \date 2020-05-20
   !> \param ep_int Electrostatic-potential integrals
   !> \param nbasis The number of AO basis functions
   !> \param points Multipole expansion points
   !> \param nPoints The number of points
   !> \param nEPcomp The number of electrostatic-potential components
   !> \param startEP The start order of the EP moments - 0 charge, 1 dipole, etc
   !> \param endEP The end order of the EP moments - 0 charge, 1 dipole, etc
   !> \param nderiv The total number of derivative components (geometric and magnetic)
   !> \param geoOrder The geometrical derivative order
   !> \param magOrder The magnetic derivative order
   !> Calculates Coulomb interaction between AO products and point multipole moments, 
   !> where (startEP, endEP) sets the range of multipole-moment orders --- endEP must 
   !> be greater than or equal to startEP
   subroutine pylsd_electrostatic_potential_integrals(ep_int, nbasis, points, nPoints, &
     &           nEPcomp, startEP, endEP, nDeriv, geoOrder, magOrder) bind(c)
   implicit none
   integer(c_int), intent(in), value :: nbasis, nPoints, nEPcomp, startEP, endEP, nDeriv, geoOrder, magOrder
   real(c_double), intent(inout)     :: ep_int(nbasis,nbasis,nPoints,nDeriv,nEPcomp)
   real(c_double), intent(in)        :: points(3, nPoints)
   !
   integer :: lupri, luerr, nbasis_, nPoints_, nEPcomp_, startEP_, endEP_, nDeriv_, geoOrder_, magOrder_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri

   nbasis_ = int(nbasis, kind=intk)
   nPoints_ = int(nPoints, kind=intk)
   nEPcomp_ = int(nEPcomp, kind=intk)
   startEP_ = int(startEP, kind=intk)
   endEP_ = int(endEP, kind=intk)
   nDeriv_ = int(nDeriv, kind=intk)
   geoOrder_ = int(geoOrder, kind=intk)
   magOrder_ = int(magOrder, kind=intk)
   !
   call LSlib_get_ep_integrals(ep_int, nbasis_, points, nPoints_, nEPcomp_, startEP_, endEP_, &
     &                         nDeriv_, geoOrder_, magOrder_, lupri, luerr)

   end subroutine pylsd_electrostatic_potential_integrals

   !> \brief Calculates the MOs by diagonalization (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-20
   !> \param C The molecular-orbital coefficients (allocated outside)
   !> \param E The molecular-orbital energies (allocated outside)
   !> \param H The AO Hamiltonian matrix (allocated outside)
   !> \param S The AO overlap matrix (allocated outside)
   !> \param nbasis The number of AO basis functions
   subroutine pylsd_diagonalize(C, E, H, S, nbasis) bind(c)
   integer(c_int), intent(in), value :: nbasis
   real(c_double), intent(inout) :: C(nbasis,nbasis)
   real(c_double), intent(inout) :: E(nbasis)
   real(c_double), intent(in) :: H(nbasis,nbasis)
   real(c_double), intent(in) :: S(nbasis,nbasis)
   !
   integer                    :: lupri, luerr, nbasis_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri
   nbasis_ = int(nbasis, kind=intk)
   call LSlib_diagonalize(C,E,H,S,nbasis_,lupri,luerr)

   end subroutine pylsd_diagonalize

   !> \brief Calculates the AO density from MOs obtained by diagonalization (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2020-02-12
   !> \param D The AO density matrix (allocated outside)
   !> \param H The AO Hamiltonian matrix (allocated outside)
   !> \param S The AO overlap matrix (allocated outside)
   !> \param nbasis The number of AO basis functions
   subroutine pylsd_diagonal_density(D, H, S, nbasis) bind(c)
   integer(c_int), intent(in), value :: nbasis
   real(c_double), intent(inout) :: D(nbasis,nbasis)
   real(c_double), intent(in) :: H(nbasis,nbasis)
   real(c_double), intent(in) :: S(nbasis,nbasis)
   !
   integer                    :: lupri, luerr, nbasis_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri
   nbasis_ = int(nbasis, kind=intk)
   call LSlib_diagonal_density(D, H, S, nbasis_, lupri, luerr)

   end subroutine pylsd_diagonal_density

   !> \brief Calculates the electron-repulsion integrals (ERIs) (from the lsdalton instance)
   !> \author S. Reine
   !> \date 2019-11-25
   !> \param eri The integral tensor
   !> \param dims The integral-tensor dimensions 5-dim (allocated outside)
   !> \param spec AO and operator specifications 
   subroutine pylsd_eri(eri, dims, spec, geoOrder, magOrder) bind(c)
   integer(c_int), intent(in)      :: dims(5)
   real(c_double)                  :: eri(dims(1),dims(2),dims(3),dims(4),dims(5))
   character(kind = c_char, len=1) :: spec(6)
   integer(c_int), intent(in), value :: geoOrder, magOrder
   !
   integer   :: lupri, luerr, dims_(5), geoOrder_, magOrder_

   luerr = LSlibconfig%luerr
   lupri = LSlibconfig%lupri
   dims_ = int(dims, kind=intk)
   geoOrder_ = int(geoOrder, kind=intk)
   magOrder_ = int(magOrder, kind=intk)
   IF (geoOrder_ == 0 .AND. magOrder_ == 0) THEN !Un-differentiated case
      call LSlib_get_eri(eri,dims_,spec(1:5),.FALSE.,lupri,luerr)
   ELSEIF (magOrder_ == 0) THEN !Geometrical-derivative case
      call LSlib_get_4center_eri_geoderiv(eri,dims_(1),geoOrder_,dims_(5),.FALSE.,lupri,luerr)
   ELSE
      call LSQUIT('Error in pylsd_eri. magOrder > 0 not implemented', lupri)
   ENDIF

   end subroutine pylsd_eri

 end module pylsdalton
