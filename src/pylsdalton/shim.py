import logging
from pathlib import Path
from typing import Tuple

import numpy as np
import sys

from ._pylsdalton import ffi, lib
from .numpy_utils import *


def version():
    return ffi.string(lib.dalton_version()).decode("ascii")


@ffi.def_extern()
def logger(message: str, level: int = logging.ERROR) -> None:
    """A primitive front-end to the logging module.

    Parameters
    ----------
    message : str
      Message to log to Python.
    level : int
      Logging level. Valid values correspond with those defined by the `logging` module.
      Default: logging.ERROR (40)
      See: https://docs.python.org/3.7/library/logging.html#logging-levels

    Notes
    -----
    """
    msg = ffi.string(message).decode("ascii")
    if level == logging.DEBUG:
        logging.debug(msg)
    elif level == logging.INFO:
        logging.info(msg)
    elif level == logging.WARNING:
        logging.warning(msg)
    elif level == logging.ERROR:
        logging.error(msg)
    else:
        logging.critical(msg)


def sample_dal(where=Path.cwd()):
    """Create a sample LSDALTON.INP file.

    Parameters
    ----------
    where : str or Path
      Where to write the file, default to the current directory.
    """
    dal = """**GENERAL
.TIME
.PSFUN
**INTEGRALS
.NOFAMILY
**WAVE FUNCTIONS
.DFT
B3LYP
*END OF INPUT
"""
    if not isinstance(where, Path):
        where = Path(where)

    p = (where / "LSDALTON.INP").resolve()
    with p.open("w") as dal_file:
        dal_file.write(dal)
    return p


def sample_mol(where=Path.cwd()):
    """Create a sample MOLECULE.INP file.

    Parameters
    ----------
    where : str or Path
      Where to write the file, default to the current directory.
    """
    mol = """BASIS
STO-3G Aux=3-21G


Atomtypes=2    Charge=0   Nosymmetry Angstrom
Charge=3.00   Atoms=1
Li    0.0000000    0.0000000   -0.0613836
Charge=1.00   Atoms=1
H     0.7756707    0.0000000    0.4871008
"""

    if not isinstance(where, Path):
        where = Path(where)

    p = (where / "MOLECULE.INP").resolve()
    with p.open("w") as mol_file:
        mol_file.write(mol)
    return p


def create(dalfile="LSDALTON.INP", molfile="MOLECULE.INP"):
    """Create an LSDALTON instance.

    Parameters
    ----------
    dalfile : str or Path
      Full path to the LSDALTON.INP file, default to the current directory.
    molfile : str or Path
      Full path to the MOLECULE.INP file, default to the current directory.
    """

    logging_format = "[%(asctime)s %(levelname)s process %(process)d] %(message)s"
    logging.basicConfig(
        format=logging_format, datefmt="%Y-%m-%d %I:%M:%S %p", level=logging.INFO
    )

    dalfile = Path(dalfile).resolve()
    if not dalfile.is_file():
        raise RuntimeError(f"{dalfile} does not exist!")

    molfile = Path(molfile).resolve()
    if not molfile.is_file():
        raise RuntimeError(f"{molfile} does not exist!")

    dal = ffi.new("char[]", str(dalfile).encode("ascii"))
    mol = ffi.new("char[]", str(molfile).encode("ascii"))
    lib.pylsd_create(dal, len(dal), mol, len(mol))


def destroy():
    """Destroy the LSDALTON instance."""
    lib.pylsd_destroy()


def number_atoms() -> int:
    """Number of atoms."""
    return lib.pylsd_natoms()


def number_basis() -> int:
    """Number of regular basis functions."""
    return lib.pylsd_nbasis()


def number_aux() -> int:
    """Number of auxiliary basis functions."""
    return lib.pylsd_nauxbasis()


def number_electrons() -> int:
    """Number of electrons."""
    return lib.pylsd_nelectrons()


def overlap_matrix(geoOrder: int = 0, magOrder: int = 0) -> np.ndarray:
    """Calculates the overlap matrix.

    Args: 
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        The overlap matrix.
    """
    lspy_test_derivative_order("overlap_matrix", geoOrder, magOrder, geoMax = 4, magMax = 1)
    return numpy_mat1_routine(lib.pylsd_overlap_matrix, geoOrder, magOrder)


def kinetic_energy_matrix(geoOrder: int = 0, magOrder: int = 0) -> np.ndarray:
    """Calculates the kinetic energy matrix.

    Args: 
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        The kinetic energy matrix.
    """
    lspy_test_derivative_order("kinetic_energy_matrix", geoOrder, magOrder, geoMax = 4, magMax = 1)
    return numpy_mat1_routine(lib.pylsd_kinetic_energy_matrix, geoOrder, magOrder)


def nuclear_electron_attraction_matrix(geoOrder: int = 0, magOrder: int = 0) -> np.ndarray:
    """Calculates the nuclear-electron attraction matrix.

    Args: 
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        The nuclear-electron attraction matrix.
    """
    lspy_test_derivative_order("nuclear_electron_attraction_matrix", geoOrder, magOrder, geoMax = 4, magMax = 1)
    return numpy_mat1_routine(lib.pylsd_nuclear_electron_attraction_matrix, geoOrder, magOrder)


def coulomb_matrix(Dmat: np.ndarray, geoOrder: int = 0, magOrder: int = 0) -> np.ndarray:
    """Calculates the Coulomb matrix.

    Args: 
        Dmat: The AO density matrices.
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        The Coulomb matrices.
    """
    lspy_test_derivative_order("coulomb_matrix", geoOrder, magOrder, geoMax = 4, magMax = 1)
    return numpy_matn_routine(lib.pylsd_coulomb_matrix, Dmat, geoOrder, magOrder)


def exchange_matrix(Dmat: np.ndarray, dsym: bool = True, testElectrons: bool = True, geoOrder: int = 0, magOrder: int = 0) -> np.ndarray:
    """Calculates the exchange matrix.

    Args: 
        Dmat: The AO density matrices.
        dsym: Whether all density matrices are symmetric. Default: True.
        testElectrons: Whether to test that the electron density integrates to the number of
                       electrons or not. Default: True.
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order


    Returns:
        The exchange matrices.
    """
    lspy_test_derivative_order("exchange_matrix", geoOrder, magOrder, geoMax = 4, magMax = 1)
    return numpy_matn_routine(lib.pylsd_exchange_matrix, Dmat, geoOrder, magOrder, dsym, testElectrons)


def exchange_correlation_matrix(Dmat: np.ndarray, testElectrons: bool=False, geoOrder: int = 0, magOrder: int = 0) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the exchange-correlation matrix and energy.

    Args: 
        Dmat: The AO density matrices.
        testElectrons: Whether to test that the electron density integrates to the number of
                       electrons or not. Default: True.
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        A vector of exchange-correlation energies.
        The exchange-correlation matrices.
    """
    lspy_test_derivative_order("exchange_correlation_matrix", geoOrder, magOrder, geoMax = 0, magMax = 0)
    return numpy_XC_matrix(lib.pylsd_exchange_correlation_matrix, Dmat, geoOrder, magOrder, testElectrons)


def fock_matrix(Dmat: np.ndarray, testElectrons: bool=False, geoOrder: int = 0, magOrder: int = 0) -> np.ndarray:
    """Calculates the Fock/Kohn-Sham matrix.

    Args: 
        Dmat: The AO density matrices.
        testElectrons: Whether to test that the electron density integrates to the number of
                       electrons or not. Default: True.
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        The Fock matrices.
    """
    lspy_test_derivative_order("fock_matrix", geoOrder, magOrder, geoMax = 0, magMax = 0)
    return numpy_matn_routine(lib.pylsd_fock_matrix, Dmat, geoOrder, magOrder, testElectrons)

def electrostatic_potential_vector(Dmat: np.ndarray, points: np.ndarray, epOrder: Tuple[int, int] = (0, 0), geoOrder: int = 0, magOrder: int = 0) -> np.ndarray:
    """Calculates the electrostatic-potential from an electron density

    Args: 
        Dmat: The AO density matrices.
        points: points where the electrostatic potential is to be evaluated
        epOrder: multipole-moment order range - 0 charge, 1 dipole, 2 quadropoles, etc.
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        Point-wise electronic multipol-moment potential

    Notes:
        Returns the electrostatic potential (order 0), dipoles (order 1), quadropoles (order 2), etc. In 
        order to calculate only the dipole potentials, choose ep_order = (1,1), to calculate both point 
        and dipole potentials choose ep_order = (0,1)
    """
    lspy_test_derivative_order("electrostatic_potential_vector", geoOrder, magOrder, geoMax = 4, magMax = 0)
    return numpy_ep_vector(Dmat, points, epOrder, geoOrder, magOrder)

def electrostatic_potential_matrix(moments: np.ndarray, points: np.ndarray, epOrder: Tuple[int, int] = (0, 0), geoOrder: int = 0, magOrder: int = 0) -> np.ndarray:
    """Calculates the one-electron AO interaction with a point-wise electrostatic potential

    Args: 
        moments: mulitpole moments
        points: points where the potential is to be evaluated
        epOrder: multipole-moment order range - 0 charge, 1 dipole, 2 quadropoles, etc.
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        one-electron AO interaction matrix

    Notes:
        Returns AO interaction with the electrostatic potential (order 0), dipoles (order 1), 
        quadropoles (order 2), etc. In order to calculate only the dipole potentials, choose 
        ep_order = (1,1), to calculate both point and dipole potentials choose ep_order = (0,1)

    """
    lspy_test_derivative_order("electrostatic_potential_matrix", geoOrder, magOrder, geoMax = 4, magMax = 0)
    return numpy_ep_matrix(moments, points, epOrder, geoOrder, magOrder)

def electrostatic_potential_integrals(points: np.ndarray, epOrder: Tuple[int, int] = (0, 0), geoOrder: int = 0, magOrder: int = 0) -> np.ndarray:
    r"""Calculates the electrostatic-potential integrals

    Args: 
        points: points where the electrostatic potential is to be evaluated
        epOrder: multipole-moment order range - 0 charge, 1 dipole, 2 quadropoles, etc.
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        electrostatic-potential interaction AO integrals

    Notes:
        Returns the electrostatic potential interaction AO integrals: (order 0), dipoles 
        (order 1), quadropoles (order 2), etc. In order to calculate only the dipole 
        potentials, choose ep_order = (1,1), to calculate both point and dipole potentials 
        choose ep_order = (0,1)


        Let a and b be AO basis functions, C a point, and :math:`\xi` and :math:`\zeta` Cartesian components, then
        order 0: :math:`(ab|C) = \int a(r) b(r) V(r,C) dr`, 
                 with the point-charge potential: :math:`V(r,C) = 1/|r-C|`
        order 1: :math:`\int a(r) b(r) V_\xi(r,C) dr`, 
                 with point-dipole potential:  :math:`V_\xi(r,C) = \partial{V(r,C)}{\partial{C_\xi}}= C_\xi/|r-C|^3`
        order 2: :math:`\int a(r) b(r) V_{\xi,\zeta}(r,C) dr`,
                 with :math:`V_{\xi,\zeta}(r,C) = \partial^2{V(r,C)}{\partial{C_\xi}\partial{C_\zeta}}`
    """
    lspy_test_derivative_order("electrostatic_potential_integrals", geoOrder, magOrder, geoMax = 4, magMax = 0)
    return numpy_ep_integrals(points, epOrder, geoOrder, magOrder)

def diagonalize(Hmat: np.ndarray, Smat: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    r"""Solves the secular equations
    .. math::
       \mathbf{H}\mathbf{C} = \mathbf{S}\mathbf{C}\epsilon

    Args:
        Hmat: The AO-basis Hamiltonian matrix.
        Smat: The AO-basis metric matrix, e.g. AO overlap matrix.

    Returns:
        The orbital energies :math:`\epsilon`.
        The molecular orbital coefficients :math:`\mathbf{C}`.

    Notes
    -----
    Using this function is equivalent to:

    .. code-block:: python

       from scipy.linalg import eigh
       E, CMO = eigh(a=Hmat, b=Smat)
       CMO = np.transpose(CMO)
    """
    return numpy_diagonalize(lib.pylsd_diagonalize, Hmat, Smat)

def diagonal_density(Hmat: np.ndarray, Smat: np.ndarray) -> np.ndarray:
    r"""Solves the secular equations
    .. math::
       \mathbf{H}\mathbf{C} = \mathbf{S}\mathbf{C}\epsilon
       
       and constructs the AO density matrix from
    .. math::
       \mathbf{D} = \mathbf{C}\mathbf{C}^T

    Args:
        Hmat: The AO-basis Hamiltonian matrix.
        Smat: The AO-basis metric matrix, e.g. AO overlap matrix.


    Returns:
        The AO density matrix

    Notes
    -----
    Using this function is equivalent to:

    .. code-block:: python

       from scipy.linalg import eigh
       E, CMO = eigh(a=Hmat, b=Smat)
       CMO_occ = CMO[0:nocc,:]
       Dmat = np.transpose(CMO_occ)*CMO_occ
    """
    return numpy_diagonal_density(lib.pylsd_diagonal_density, Hmat, Smat)

def eri(specs: str, geoOrder: int = 0, magOrder: int = 0) -> np.ndarray:
    r"""Calculates electron-repulsion integrals.
    .. math::
       g_{ijkl} = \int\mathrm{d}\mathbf{r}\int\mathrm{d}\mathbf{r}^{\prime}
                  \chi_{i}(\mathbf{r})\chi_{j}(\mathbf{r}^\prime)
                  g(\mathbf{r},\mathbf{r}^\prime)
                  \chi_{k}(\mathbf{r})\chi_{l}(\mathbf{r}^\prime)

    Args: 
        specs: A 5-character string with the specification for AO basis and operator to use.
        geoOrder: Geometrical-derivative order
        magOrder: Magnetic-derivative order

    Returns:
        The AO electron-repulsion integral tensor.

    Notes
    -----
    The first 4 characters in the `specs` input parameter give the AO basis to
    use for the 4 basis functions: :math:`\chi_{i}`, :math:`\chi_{j}`,
    :math:`\chi_{k}`, and :math:`\chi_{l}`. Valid values are:
      * R for regular AO basis
      * D for auxiliary AO basis (for RI)
      * E for empty AO basis (i.e. for 2- and 3-center ERIs)
      * N for nuclei

    The last character in the `specs` string gives the operator to use:
    :math:`g(\mathbf{r}, \mathbf{r}^\prime)`. Valid values are:
      * C for Coulomb, :math:`g(\mathbf{r}, \mathbf{r}^\prime) = \frac{1}{|\mathbf{r} - \mathbf{r}^\prime|}`
      * G for Gaussian geminal, g
      * F geminal divided by the Coulomb operator g/r12
      * D double communtator [[T,g],g]
      * 2 Gaussian geminal operator squared g^2
    """
    dims = [1, 1, 1, 1, 1]
    for i in range(4):
        if specs[i] == "R":
            dims[i] = lib.pylsd_nbasis()
        elif specs[i] == "D":
            dims[i] = lib.pylsd_nauxbasis()
        elif specs[i] == "E":
            dims[i] = int(1)
        elif specs[i] == "N":
            dims[i] = lib.pylsd_natom()
        else:
            raise TypeError("Unknown AO specifier ", specs[i])
    if specs[4] not in "CGFD2":
        raise TypeError("Unknown operator specifier ", specs[4])

    nDeriv = numpy_nderiv(geoOrder, magOrder)
    dims[4] = nDeriv

    eris = np.zeros(shape=tuple(dims[::-1]), dtype=np.float64)
    dim = ffi.new("int[5]", dims)
    spec = ffi.new("char[5]", specs.encode("ascii"))
    eri_ptr = ffi.from_buffer("double*", eris, require_writable=True)
    lspy_test_derivative_order("eri", geoOrder, magOrder, geoMax = 4, magMax = 0)
    lib.pylsd_eri(eri_ptr, dim, spec, geoOrder, magOrder)
    if nDeriv == 1:
        return np.reshape(eris, (dims[3], dims[2], dims[1], dims[0]))
    else:
        return eris
