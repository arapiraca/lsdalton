/* CFFI would issue warning with pragma once */
#ifndef PYLSDALTON_H_INCLUDED
#define PYLSDALTON_H_INCLUDED

// This gives us bool as a type in C
#include <stdbool.h>

#ifndef PYLSDALTON_API
#include "pylsdalton_export.h"
#define PYLSDALTON_API PYLSDALTON_EXPORT
#endif

#ifdef __cplusplus
extern "C" {
#endif

PYLSDALTON_API
const char *dalton_version();

PYLSDALTON_API
void pylsd_create(const char *dalfile, int len_dalfile, const char *molfile,
                 int len_molfile);

PYLSDALTON_API
void pylsd_destroy();

PYLSDALTON_API
int pylsd_natoms();

PYLSDALTON_API
int pylsd_nbasis();

PYLSDALTON_API
int pylsd_nelectrons();

PYLSDALTON_API
int pylsd_nauxbasis();

PYLSDALTON_API
void pylsd_overlap_matrix(double *S, int nbasis, int nDeriv, int geoOrder, int magOrder);

PYLSDALTON_API
void pylsd_kinetic_energy_matrix(double *k, int nbasis, int nDeriv, int geoOrder, int magOrder);

PYLSDALTON_API
void pylsd_nuclear_electron_attraction_matrix(double *h, int nbasis, int nDeriv, int geoOrder, int magOrder);

PYLSDALTON_API
void pylsd_coulomb_matrix(double *J, const double *D, int nbasis, int ndmat, int nDeriv, int geoOrder, int magOrder);

PYLSDALTON_API
void pylsd_exchange_matrix(double *K, const double *D, int nbasis, int ndmat,
                           int nDeriv, int geoOrder, int magOrder, bool dsym, bool testElectrons);

PYLSDALTON_API
void pylsd_exchange_correlation_matrix(double *X, const double *D, int nbasis,
                                      int ndmat, int nDeriv, int geoOrder, int magOrder, 
                                      double *EXC, bool testElectrons);

PYLSDALTON_API
void pylsd_fock_matrix(double *F, const double *D, int nbasis, int ndmat,
                       int nDeriv, int geoOrder, int magOrder, bool testElectrons);

PYLSDALTON_API
void pylsd_diagonalize(double *C, double *E, const double *H, const double *S,
                      int nbasis);

PYLSDALTON_API
void pylsd_diagonal_density(double *D, const double *H, const double *S,
                      int nbasis);

PYLSDALTON_API
void pylsd_eri(double *eri, const int *dims, const char spec[5], int geoOrder, int magOrder);

PYLSDALTON_API
void pylsd_electrostatic_potential_matrix(double *ep_mat, const double *moments, int nbasis, 
                                          double *points, int nPoints, int nEPcomp, int startEP,
                                          int endEP, int nDeriv, int geoOrder, int magOrder);

PYLSDALTON_API
void pylsd_electrostatic_potential_vector(double *ep_vec, const double *D, int nbasis, int ndmat,
                                          double *points, int nPoints, int nEPcomp, int startEP,
                                          int endEP, int nDeriv, int geoOrder, int magOrder);

PYLSDALTON_API
void pylsd_electrostatic_potential_integrals(double *ep_int, int nbasis, 
                                             double *points, int nPoints, int nEPcomp, int startEP,
                                             int endEP, int nDeriv, int geoOrder, int magOrder);

#ifdef __cplusplus
}
#endif

#endif /* PYLSDALTON_H_INCLUDED */
