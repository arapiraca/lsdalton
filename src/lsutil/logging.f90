module logging
  implicit none

  private

  interface
     subroutine logger_t(message, level)
       implicit none
       character(len=*), intent(in) :: message
       integer, intent(in) :: level
     end subroutine
  end interface

  procedure(logger_t), pointer, save :: pLogger => null()
  logical, save :: is_initialized = .false.

  public logger_init
  public logger_write

contains

  subroutine logger_init(l)
    procedure(logger_t) :: l

    pLogger => l
    is_initialized = .true.
  end subroutine

  subroutine logger_write(message, level)
    character(len=*), intent(in) :: message
    integer, intent(in) :: level

    ! Do nothing is the logger was not initialized
    if (is_initialized) then
       call pLogger(message, level)
    end if
  end subroutine

end module

subroutine log_debug(message)
  use logging, only: logger_write
  implicit none
  character(len=*), intent(in) :: message

  call logger_write(message, 10)
end subroutine

subroutine log_info(message)
  use logging, only: logger_write
  implicit none
  character(len=*), intent(in) :: message

  call logger_write(message, 20)
end subroutine

subroutine log_warning(message)
  use logging, only: logger_write
  implicit none
  character(len=*), intent(in) :: message

  call logger_write(message, 30)
end subroutine

subroutine log_error(message)
  use logging, only: logger_write
  implicit none
  character(len=*), intent(in) :: message

  call logger_write(message, 40)
end subroutine

subroutine log_critical(message)
  use logging, only: logger_write
  implicit none
  character(len=*), intent(in) :: message

  call logger_write(message, 50)
end subroutine
