module lsmpi_Bcast
  use lsmpi_param
  use precision
  use lsmpi_typeParam
  use,intrinsic :: iso_c_binding,only:c_ptr,c_f_pointer,c_associated,&
       & c_null_ptr,c_loc
  use LSparameters
  use memory_handling, only: mem_alloc,mem_dealloc, max_mem_used_global,&
       & longintbuffersize, print_maxmem, stats_mem, copy_from_mem_stats,&
       & init_globalmemvar, stats_mpi_mem, copy_to_mem_stats, &
       & MemModParamPrintMemory, Print_Memory_info, &
       & MemModParamPrintMemorylupri, mem_allocated_global
#ifdef VAR_MPI
  use infpar_module
  use lsmpi_module
#endif

  INTERFACE ls_mpibcast_chunks
     MODULE PROCEDURE ls_mpibcast_realkV_parts44,ls_mpibcast_realkV_parts48,&
          & ls_mpibcast_realkV_parts84,ls_mpibcast_realkV_parts88
  END INTERFACE ls_mpibcast_chunks

  INTERFACE ls_mpibcast
     MODULE PROCEDURE ls_mpibcast_integer,& 
          &           ls_mpibcast_integerV,ls_mpibcast_integerV_wrapper8,&
          &           ls_mpibcast_longV, ls_mpibcast_longV_wrapper8, &
          &           ls_mpibcast_integerM,ls_mpibcast_integerM_wrapper8,&
          &           ls_mpibcast_longM, ls_mpibcast_longM_wrapper8, &
          &           ls_mpibcast_realk, ls_mpibcast_realkV,&
          &           ls_mpibcast_realkV_wrapper8,&
          &           ls_mpibcast_realkM, ls_mpibcast_realkT,&
          &           ls_mpibcast_realkQ,&
          &           ls_mpibcast_logical4,ls_mpibcast_logical8,&
          &           ls_mpibcast_logical4V,ls_mpibcast_logical4V_wrapper8, &
          &           ls_mpibcast_logical8V,ls_mpibcast_logical8V_wrapper8, &
          &           ls_mpibcast_logical4M4,ls_mpibcast_logical4M8,&
          &           ls_mpibcast_logical8M4,ls_mpibcast_logical8M8,&
          &           ls_mpibcast_short, &
          &           ls_mpibcast_charac, &
          &           ls_mpibcast_characV,ls_mpibcast_characV_wrapper8, &
          &           ls_mpibcast_characV2,ls_mpibcast_characV2_wrapper8, &
          &           ls_mpibcast_shortV,ls_mpibcast_shortV_wrapper8,&
          &           ls_mpibcast_long
  END INTERFACE ls_mpibcast

  public :: ls_mpibcast_chunks,ls_mpibcast

  private

contains
  ! ########################################################################
  !                          MPI BCAST
  ! ########################################################################
  subroutine ls_mpibcast_integer(buffer,master,comm)
    implicit none
    integer(kind=4) :: buffer
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: DATATYPE,n,IERR
    IERR=0
    DATATYPE = MPI_INTEGER4
    n = 1
    CALL MPI_BCAST(BUFFER,n,DATATYPE,master,comm,IERR)
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine ls_mpibcast_integer

  subroutine ls_mpibcast_long(buffer,master,comm)
    implicit none
    integer(kind=8) :: buffer
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: DATATYPE,n,IERR
    IERR=0
    DATATYPE = MPI_INTEGER8
    n = 1
    CALL MPI_BCAST(BUFFER,n,DATATYPE,master,comm,IERR)
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine ls_mpibcast_long

  subroutine ls_mpibcast_short(buffer,master,comm)
    implicit none
    integer(kind=short) :: buffer
    integer(kind=ls_mpik) :: master
    integer(kind=ls_mpik) :: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,n,datatype
    integer(kind=4) :: intbuffer
    IERR=0
    DATATYPE = MPI_INTEGER4
    n = 1
    !     Convert from short integer to regular integer
    intbuffer = buffer
    !     Broadcast regular integer
    CALL MPI_BCAST(INTBUFFER,n,DATATYPE,master,comm,IERR)
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    !     Convert back
    buffer = INT(intbuffer,kind=short)
#endif
  end subroutine ls_mpibcast_short

  subroutine ls_mpibcast_shortV_wrapper8(buffer,n,master,comm)
    implicit none
    integer(kind=ls_mpik),intent(in) :: comm
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=8) :: n
    integer(kind=short) :: buffer(:)
#ifdef VAR_MPI
    integer(kind=8) :: i
    integer(kind=4),pointer :: intbuffer(:)
    call mem_alloc(intbuffer,n)
    do i=1,n
       intbuffer(i) = buffer(i)
    enddo
    call ls_mpibcast(intbuffer,n,master,comm)
    do i=1,n
       buffer(i) = INT(intbuffer(i),kind=short)
    enddo
    call mem_dealloc(intbuffer)
#endif
  end subroutine ls_mpibcast_shortV_wrapper8

  subroutine ls_mpibcast_shortV(buffer,n,master,comm)
    implicit none
    integer(kind=short)  :: buffer(:)
    integer(kind=ls_mpik):: master
    integer(kind=4)      :: n
    integer(kind=ls_mpik):: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=8) :: n1
    n1 = n
    call ls_mpibcast_shortV_wrapper8(buffer,n1,master,comm)
#endif
  end subroutine ls_mpibcast_shortV

  subroutine ls_mpibcast_longV_wrapper8(buffer,n,master,comm)
    implicit none
    integer(kind=ls_mpik),intent(in) :: comm
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=8) :: n
    integer(kind=8) :: buffer(:)
#ifdef VAR_MPI
    integer(kind=8) :: i,k
    integer(kind=ls_mpik) :: ierr,datatype,nMPI
    DATATYPE = MPI_INTEGER8
    IERR=0
    k=SPLIT_MPI_MSG
    do i=1,n,k
       nMPI=k
       !if((n-i)<k)nMPI=mod(n,k)
       if(((n-i)<k).and.(mod(n-i+1,k)/=0))nMPI=mod(n,k)
       CALL MPI_BCAST(BUFFER(i:i+nMPI-1),nMPI,DATATYPE,master,comm,IERR)
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif
  end subroutine ls_mpibcast_longV_wrapper8

  subroutine ls_mpibcast_longV(buffer,n,master,comm)
    implicit none
    integer(kind=ls_mpik),intent(in) :: comm
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=4) :: n
    integer(kind=8) :: buffer(:)
#ifdef VAR_MPI
    integer(kind=8) :: n1
    n1 = n
    call ls_mpibcast_longV_wrapper8(buffer,n1,master,comm)
#endif
  end subroutine ls_mpibcast_longV

  subroutine ls_mpibcast_integerV_wrapper8(buffer,n,master,comm)
    implicit none
    integer(kind=ls_mpik),intent(in) :: comm
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=8) :: n
    integer(kind=4) :: buffer(:)
#ifdef VAR_MPI
    integer(kind=8) :: i,k
    integer(kind=ls_mpik) :: ierr,datatype,nMPI
    DATATYPE = MPI_INTEGER4
    IERR=0
    k=SPLIT_MPI_MSG
    do i=1,n,k
       nMPI=k
       !if((n-i)<k)nMPI=mod(n,k)
       if(((n-i)<k).and.(mod(n-i+1,k)/=0))nMPI=mod(n,k)
       CALL MPI_BCAST(BUFFER(i:i+nMPI-1),nMPI,DATATYPE,master,comm,IERR)
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif
  end subroutine ls_mpibcast_integerV_wrapper8

  subroutine ls_mpibcast_integerV(buffer,n,master,comm)
    implicit none
    integer(kind=4)       :: n
    integer(kind=ls_mpik) :: master
    integer(kind=4)       :: buffer(:)
    integer(kind=ls_mpik) :: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=8) :: n1
    n1 = n
    call ls_mpibcast_integerV_wrapper8(buffer,n1,master,comm)
#endif
  end subroutine ls_mpibcast_integerV

  subroutine ls_mpibcast_longM_wrapper8(buffer,n1,n2,master,comm)
    implicit none
    integer(kind=ls_mpik),intent(in) :: comm
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=8), intent(in)     :: n1,n2
    integer(kind=8),target :: buffer(n1,n2)
#ifdef VAR_MPI
    integer(kind=8),pointer :: buffertmp(:)
    integer(kind=8) :: i,k,n
    integer(kind=ls_mpik) :: ierr,datatype,nMPI
    IERR=0
    DATATYPE = MPI_INTEGER8
    n=n1*n2
#ifdef VAR_PTR_RESHAPE
    buffertmp(1:(i8*n1)*n2) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1)),buffertmp,[(i8*n1)*n2])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    k=SPLIT_MPI_MSG
    do i=1,n,k
       nMPI=k
       if(((n-i)<k).and.(mod(n-i+1,k)/=0))nMPI=mod(n,k)
       CALL MPI_BCAST(BUFFERTMP(i:i+nMPI-1),nMPI,DATATYPE,master,comm,IERR)
    enddo
    nullify(buffertmp)
#endif
  end subroutine ls_mpibcast_longM_wrapper8

  subroutine ls_mpibcast_longM(buffer,n1,n2,master,comm)
    implicit none
    integer(kind=8) :: buffer(:,:)
    integer(kind=4) :: n1,n2
    integer(kind=ls_mpik) :: master
    integer(kind=ls_mpik) :: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=8) :: n1_8,n2_8
    n1_8 = n1
    n2_8 = n2
    call ls_mpibcast_longM_wrapper8(buffer,n1_8,n2_8,master,comm)
#endif
  end subroutine ls_mpibcast_longM

  subroutine ls_mpibcast_integerM_wrapper8(buffer,n1,n2,master,comm)
    implicit none
    integer(kind=ls_mpik),intent(in) :: comm
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=8) :: n1,n2
    integer(kind=4),target :: buffer(n1,n2)
#ifdef VAR_MPI
    integer(kind=4),pointer :: buffertmp(:)
    integer(kind=8) :: i,k,n
    integer(kind=ls_mpik) :: ierr,datatype,nMPI
    IERR=0
    DATATYPE = MPI_INTEGER4
    n=n1*n2
#ifdef VAR_PTR_RESHAPE
    buffertmp(1:(i8*n1)*n2) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1)),buffertmp,[(i8*n1)*n2])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    k=SPLIT_MPI_MSG
    do i=1,n,k
       nMPI=k
       if(((n-i)<k).and.(mod(n-i+1,k)/=0))nMPI=mod(n,k)
       CALL MPI_BCAST(BUFFERTMP(i:i+nMPI-1),nMPI,DATATYPE,master,comm,IERR)
    enddo
    nullify(buffertmp)
#endif
  end subroutine ls_mpibcast_integerM_wrapper8

  subroutine ls_mpibcast_integerM(buffer,n1,n2,master,comm)
    implicit none
    integer(kind=4)       :: n1,n2
    integer(kind=ls_mpik) :: master
    integer(kind=4)       :: buffer(:,:)
    integer(kind=ls_mpik) :: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=8) :: n1_8,n2_8
    n1_8=n1
    n2_8=n2
    call ls_mpibcast_integerM_wrapper8(buffer,n1_8,n2_8,master,comm)
#endif
  end subroutine ls_mpibcast_integerM

  subroutine ls_mpibcast_realk(buffer,master,comm)
    implicit none
    real(realk) :: buffer
    integer(kind=ls_mpik) :: master
    integer(kind=ls_mpik) :: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,datatype,cnt
    IERR=0
    DATATYPE = MPI_DOUBLE_PRECISION
    cnt = 1
    CALL MPI_BCAST(BUFFER,cnt,DATATYPE,master,comm,IERR)
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine ls_mpibcast_realk

  subroutine ls_mpibcast_realkV_wrapper8(buffer,n,master,comm)
    implicit none
    integer(kind=ls_mpik) :: master
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=8) :: n
    real(realk) :: buffer(:)
#ifdef VAR_MPI
    integer(kind=8) :: i,k
    integer(kind=ls_mpik) :: ierr,datatype,nMPI
    IERR=0
    DATATYPE = MPI_DOUBLE_PRECISION
    k=SPLIT_MPI_MSG
    do i=1,n,k
       nMPI=k
       if(((n-i)<k).and.(mod(n-i+1,k)/=0))nMPI=mod(n,k)
       CALL MPI_BCAST(BUFFER(i:i+nMPI-1),nMPI,DATATYPE,master,comm,IERR)
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif
  end subroutine ls_mpibcast_realkV_wrapper8

  subroutine ls_mpibcast_realkV(buffer,n,master,comm)
    implicit none
    integer(kind=ls_mpik) :: master
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=4) :: n
    real(realk) :: buffer(:)
#ifdef VAR_MPI
    integer(kind=8) :: n1
    n1 = n
    call ls_mpibcast_realkV_wrapper8(buffer,n1,master,comm)
#endif
  end subroutine ls_mpibcast_realkV

  subroutine ls_mpibcast_realkM(buffer,n1,n2,master,comm)
    implicit none
    integer :: n1,n2
    integer(kind=ls_mpik) :: master
    real(realk),target :: buffer(n1,n2)
    integer(kind=ls_mpik) :: comm   ! communicator
#ifdef VAR_MPI
    real(realk),pointer :: buf(:)
    integer(kind=8) :: i,k,n
    integer(kind=ls_mpik) :: ierr,datatype,nMPI
    IERR=0
    DATATYPE = MPI_DOUBLE_PRECISION
    n=n1*n2
#ifdef VAR_PTR_RESHAPE
    buf(1:(i8*n1)*n2) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1)),buf,[(i8*n1)*n2])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    k=SPLIT_MPI_MSG
    do i=1,n,k
       nMPI=k
       if(((n-i)<k).and.(mod(n-i+1,k)/=0))nMPI=mod(n,k)
       CALL MPI_BCAST(BUF(i:i+nMPI-1),nMPI,DATATYPE,master,comm,IERR)
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
    nullify(buf)
#endif
  end subroutine ls_mpibcast_realkM

  subroutine ls_mpibcast_realkT(buffer,n1,n2,n3,master,comm)
    implicit none
    integer(kind=ls_mpik) :: master
    integer :: n1,n2,n3
    real(realk),target :: buffer(n1,n2,n3)
    integer(kind=ls_mpik) :: comm   ! communicator
#ifdef VAR_MPI
    real(realk),pointer :: buf(:)
#ifdef VAR_PTR_RESHAPE
    buf(1:(i8*n1)*n2*n3) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1,1)),buf,[(i8*n1)*n2*n3])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpibcast_realkV_wrapper8(buf,((i8*n1)*n2)*n3,master,comm)
    buf => null()
#endif
  end subroutine ls_mpibcast_realkT

  subroutine ls_mpibcast_realkQ(buffer,n1,n2,n3,n4,master,comm)
    implicit none
    integer(kind=ls_mpik) :: master
    integer(kind=ls_mpik) :: comm   ! communicator
    integer :: n1,n2,n3,n4
    real(realk),target :: buffer(n1,n2,n3,n4)
    real(realk),pointer :: buf(:)
#ifdef VAR_MPI
#ifdef VAR_PTR_RESHAPE
    buf(1:(i8*n1)*n2*n3*n4) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1,1,1)),buf,[(i8*n1)*n2*n3*n4])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpibcast_realkV_wrapper8(buf,(((i8*n1)*n2)*n3)*n4,master,comm)
    buf => null()
#endif
  end subroutine ls_mpibcast_realkQ

  subroutine ls_mpibcast_logical4(buffer,master,comm)
    implicit none
    logical(kind=4),intent(inout) :: buffer
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,cnt,datatype
    logical(kind=8) :: buffer8
    integer(kind=MPI_ADDRESS_KIND) :: mpi_logical_extent,lb
    IERR=0
    DATATYPE = MPI_LOGICAL
    cnt = 1
    call MPI_TYPE_GET_EXTENT(MPI_LOGICAL,lb,mpi_logical_extent,ierr)
    IF(mpi_logical_extent.EQ.4)THEN
       !32 bit mpi logical
       CALL MPI_BCAST(BUFFER,cnt,DATATYPE,master,comm,IERR)         
    ELSE
       !64 bit mpi logical
       BUFFER8 = BUFFER
       CALL MPI_BCAST(BUFFER8,cnt,DATATYPE,master,comm,IERR)         
       BUFFER = BUFFER8
    ENDIF
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine ls_mpibcast_logical4

  subroutine ls_mpibcast_logical8(buffer,master,comm)
    implicit none
    logical(kind=8),intent(inout) :: buffer
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,datatype,cnt
    integer(kind=MPI_ADDRESS_KIND) :: mpi_logical_extent,lb
    logical(kind=4) :: buffer4
    DATATYPE = MPI_LOGICAL
    cnt = 1
    IERR=0
    call MPI_TYPE_GET_EXTENT(MPI_LOGICAL,lb,mpi_logical_extent,ierr)
    IF(mpi_logical_extent.EQ.4)THEN
       !32 bit mpi logical
       BUFFER4 = BUFFER
       CALL MPI_BCAST(BUFFER4,cnt,DATATYPE,master,comm,IERR)         
       BUFFER = BUFFER4
    ELSE
       !64 bit mpi logical
       CALL MPI_BCAST(BUFFER,cnt,DATATYPE,master,comm,IERR)         
    ENDIF
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine ls_mpibcast_logical8

  subroutine ls_mpibcast_logical8V_wrapper8(buffer,n,master,comm)
    implicit none
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
    logical(kind=8),intent(inout) :: buffer(:)
    integer(kind=8),intent(in) :: n
#ifdef VAR_MPI
    integer(kind=8) :: i,k
    integer(kind=ls_mpik) :: ierr,cnt,datatype,nMPI
    logical(kind=4),pointer :: buffer4(:)
    integer(kind=MPI_ADDRESS_KIND) :: mpi_logical_extent,lb
    IERR=0
    DATATYPE = MPI_LOGICAL
    call MPI_TYPE_GET_EXTENT(MPI_LOGICAL,lb,mpi_logical_extent,ierr)
    IF(mpi_logical_extent.EQ.4)THEN
       !32 bit mpi logical
       call mem_alloc(BUFFER4,n)
       DO I=1,n
          BUFFER4(I) = BUFFER(I)
       ENDDO
       k=SPLIT_MPI_MSG
       do i=1,n,k
          nMPI=k
          if(((n-i)<k).and.(mod(n-i+1,k)/=0))nMPI=mod(n,k)
          CALL MPI_BCAST(BUFFER4(i:i+nMPI-1),nMPI,DATATYPE,master,comm,IERR)         
          IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
       enddo
       DO I=1,n
          BUFFER(I) = BUFFER4(I)
       ENDDO
       call mem_dealloc(BUFFER4)
    ELSE
       !64 bit mpi logical
       k=SPLIT_MPI_MSG
       do i=1,n,k
          nMPI=k
          if(((n-i)<k).and.(mod(n-i+1,k)/=0))nMPI=mod(n,k)
          CALL MPI_BCAST(BUFFER(i:i+nMPI-1),nMPI,DATATYPE,master,comm,IERR)         
          IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
       enddo
    ENDIF
#endif
  end subroutine ls_mpibcast_logical8V_wrapper8

  subroutine ls_mpibcast_logical8V(buffer,n,master,comm)
    implicit none
    integer(kind=4),intent(in) :: n
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
    logical(kind=8),intent(inout) :: buffer(:)
#ifdef VAR_MPI
    integer(kind=8) :: n1
    n1 = n
    call ls_mpibcast_logical8V_wrapper8(buffer,n1,master,comm)
#endif
  end subroutine ls_mpibcast_logical8V

  subroutine ls_mpibcast_logical4V_wrapper8(buffer,n,master,comm)
    implicit none
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
    logical(kind=4),intent(inout) :: buffer(:)
    integer(kind=8),intent(in) :: n
#ifdef VAR_MPI
    integer(kind=8) :: i,k
    integer(kind=ls_mpik) :: ierr,cnt,datatype,nMPI
    logical(kind=8),pointer :: buffer8(:)
    integer(kind=MPI_ADDRESS_KIND) :: mpi_logical_extent,lb
    IERR=0
    DATATYPE = MPI_LOGICAL
    call MPI_TYPE_GET_EXTENT(MPI_LOGICAL,lb,mpi_logical_extent,ierr)
    IF(mpi_logical_extent.EQ.4)THEN
       !32 bit mpi logical
       k=SPLIT_MPI_MSG
       do i=1,n,k
          nMPI=k
          if(((n-i)<k).and.(mod(n-i+1,k)/=0))nMPI=mod(n,k)
          CALL MPI_BCAST(BUFFER(i:i+nMPI-1),nMPI,DATATYPE,master,comm,IERR)
          IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
       enddo
    ELSE
       !64 bit mpi logical
       call mem_alloc(BUFFER8,n)
       DO I=1,n
          BUFFER8(I) = BUFFER(I)
       ENDDO
       k=SPLIT_MPI_MSG
       do i=1,n,k
          nMPI=k
          if(((n-i)<k).and.(mod(n-i+1,k)/=0))nMPI=mod(n,k)
          CALL MPI_BCAST(BUFFER8(i:i+nMPI-1),nMPI,DATATYPE,master,comm,IERR)         
          IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
       enddo
       DO I=1,n
          BUFFER(I) = BUFFER8(I)
       ENDDO
       call mem_dealloc(BUFFER8)
    ENDIF
#endif
  end subroutine ls_mpibcast_logical4V_wrapper8

  subroutine ls_mpibcast_logical4V(buffer,n,master,comm)
    implicit none
    integer(kind=4),intent(in) :: n
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
    logical(kind=4),intent(inout) :: buffer(:)
#ifdef VAR_MPI
    integer(kind=8) :: n1
    n1 = n
    call ls_mpibcast_logical4V_wrapper8(buffer,n1,master,comm)
#endif
  end subroutine ls_mpibcast_logical4V

  subroutine ls_mpibcast_logical4M4(buffer,n1,n2,master,comm)
    implicit none
    integer(kind=4),intent(in) :: n1,n2
    integer(kind=ls_mpik),intent(in) :: master
    logical(kind=4),target,intent(inout) :: buffer(n1,n2)
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
#ifdef VAR_MPI
    logical(kind=4),pointer :: buf(:)
    integer(kind=8) :: n
    n=n1*n2
#ifdef VAR_PTR_RESHAPE
    buf(1:n) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1)),buf,[n])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpibcast_logical4V_wrapper8(buf,n,master,comm)
    nullify(buf)
#endif
  end subroutine ls_mpibcast_logical4M4

  subroutine ls_mpibcast_logical4M8(buffer,n1,n2,master,comm)
    implicit none
    integer(kind=8),intent(in) :: n1,n2
    integer(kind=ls_mpik),intent(in) :: master
    logical(kind=4),target,intent(inout) :: buffer(n1,n2)
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
#ifdef VAR_MPI
    logical(kind=4),pointer :: buf(:)
    integer(kind=8) :: n
    n=n1*n2
#ifdef VAR_PTR_RESHAPE
    buf(1:n) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1)),buf,[n])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpibcast_logical4V_wrapper8(buf,n,master,comm)
    nullify(buf)
#endif
  end subroutine ls_mpibcast_logical4M8
  subroutine ls_mpibcast_logical8M4(buffer,n1,n2,master,comm)
    implicit none
    integer(kind=4),intent(in) :: n1,n2
    integer(kind=ls_mpik),intent(in) :: master
    logical(kind=8),target,intent(inout) :: buffer(n1,n2)
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
#ifdef VAR_MPI
    logical(kind=8),pointer :: buf(:)
    integer(kind=8) :: n
    n=n1*n2
#ifdef VAR_PTR_RESHAPE
    buf(1:n) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1)),buf,[n])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpibcast_logical8V_wrapper8(buf,n,master,comm)
    nullify(buf)
#endif
  end subroutine ls_mpibcast_logical8M4
  subroutine ls_mpibcast_logical8M8(buffer,n1,n2,master,comm)
    implicit none
    integer(kind=8),intent(in) :: n1,n2
    integer(kind=ls_mpik),intent(in) :: master
    logical(kind=8),target,intent(inout) :: buffer(n1,n2)
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
#ifdef VAR_MPI
    logical(kind=8),pointer :: buf(:)
    integer(kind=8) :: n
    n=n1*n2
#ifdef VAR_PTR_RESHAPE
    buf(1:n) => buffer
#elif defined(COMPILER_UNDERSTANDS_FORTRAN_2003)
    call c_f_pointer(c_loc(buffer(1,1)),buf,[n])
#else
    call lsquit("ERROR, YOUR COMPILER IS NOT F2003 COMPATIBLE",-1)
#endif
    call ls_mpibcast_logical8V_wrapper8(buf,n,master,comm)
    nullify(buf)
#endif
  end subroutine ls_mpibcast_logical8M8

  subroutine ls_mpibcast_charac(buffer,master,comm)
    implicit none
    character :: buffer
    integer(kind=ls_mpik) :: master
    integer(kind=ls_mpik) :: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=ls_mpik) :: ierr,cnt,datatype
    IERR=0
    DATATYPE = MPI_CHARACTER
    cnt = 1
    CALL MPI_BCAST(BUFFER,cnt,DATATYPE,master,comm,IERR)
    IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
#endif
  end subroutine ls_mpibcast_charac

  subroutine ls_mpibcast_characV_wrapper8(buffer,n,master,comm)
    implicit none
    integer(kind=ls_mpik),intent(in) :: comm
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=8) :: n
    character*(*) :: buffer
#ifdef VAR_MPI
    integer(kind=8) :: i,k
    integer(kind=ls_mpik) :: ierr,datatype,nMPI
    IERR=0
    DATATYPE = MPI_CHARACTER
    k=SPLIT_MPI_MSG
    do i=1,n,k
       nMPI=k
       if(((n-i)<k).and.(mod(n-i+1,k)/=0))nMPI=mod(n,k)
       CALL MPI_BCAST(BUFFER(i:i+nMPI-1),nMPI,DATATYPE,master,comm,IERR)
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif
  end subroutine ls_mpibcast_characV_wrapper8

  subroutine ls_mpibcast_characV(buffer,n,master,comm)
    implicit none
    integer(kind=4) :: n
    integer(kind=ls_mpik),intent(in) :: master
    character*(*) :: buffer
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=8) :: n1
    n1 = n
    call ls_mpibcast_characV_wrapper8(buffer,n1,master,comm)
#endif
  end subroutine ls_mpibcast_characV

  subroutine ls_mpibcast_characV2_wrapper8(buffer,n,master,comm)
    implicit none
    integer(kind=ls_mpik),intent(in) :: comm
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=8) :: n
    character :: buffer(n)
#ifdef VAR_MPI
    integer(kind=8) :: i,k
    integer(kind=ls_mpik) :: ierr,datatype,nMPI
    IERR=0
    DATATYPE = MPI_CHARACTER
    k=SPLIT_MPI_MSG
    do i=1,n,k
       nMPI=k
       if(((n-i)<k).and.(mod(n-i+1,k)/=0))nMPI=mod(n,k)
       CALL MPI_BCAST(BUFFER(i:i+nMPI-1),nMPI,DATATYPE,master,comm,IERR)
       IF (IERR.GT. 0) CALL LSMPI_MYFAIL(IERR)
    enddo
#endif
  end subroutine ls_mpibcast_characV2_wrapper8

  subroutine ls_mpibcast_characV2(buffer,n,master,comm)
    implicit none
    integer(kind=4),intent(in) :: n
    character,intent(inout) :: buffer(:)
    integer(kind=ls_mpik),intent(in) :: master
    integer(kind=ls_mpik),intent(in) :: comm   ! communicator
#ifdef VAR_MPI
    integer(kind=8) :: n1
    n1 = n
    call ls_mpibcast_characV2_wrapper8(buffer,n1,master,comm)
#endif
  end subroutine ls_mpibcast_characV2

  subroutine ls_mpibcast_realkV_parts44(buffer,nelms,master,comm,batchsze)
    implicit none
    integer(kind=ls_mpik) :: master
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=4),intent(in) :: batchsze
    integer(kind=4),intent(in) :: nelms
    real(realk) :: buffer(:)
#ifdef VAR_MPI
    integer(kind=8) :: i,n
    do i=1,nelms,batchsze
       n=batchsze
       if(((nelms-i)<batchsze).and.&
            &(mod(nelms-i+1,batchsze)/=0))n=mod(nelms,batchsze)
       call ls_mpibcast_realkV_wrapper8(buffer(i:i+n-1),n,master,comm)
    enddo
#endif
  end subroutine ls_mpibcast_realkV_parts44

  subroutine ls_mpibcast_realkV_parts48(buffer,nelms,master,comm,batchsze)
    implicit none
    integer(kind=ls_mpik) :: master
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=4),intent(in) :: batchsze
    integer(kind=8),intent(in) :: nelms
    real(realk) :: buffer(:)
#ifdef VAR_MPI
    integer(kind=8) :: i,n
    do i=1,nelms,batchsze
       n=batchsze
       if(((nelms-i)<batchsze).and.&
            &(mod(nelms-i+1,batchsze)/=0))n=mod(nelms,batchsze)
       call ls_mpibcast_realkV_wrapper8(buffer(i:i+n-1),n,master,comm)
    enddo
#endif
  end subroutine ls_mpibcast_realkV_parts48

  subroutine ls_mpibcast_realkV_parts84(buffer,nelms,master,comm,batchsze)
    implicit none
    integer(kind=ls_mpik) :: master
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=8),intent(in) :: batchsze
    integer(kind=4),intent(in) :: nelms
    real(realk) :: buffer(:)
#ifdef VAR_MPI
    integer(kind=8) :: i,n
    do i=1,nelms,batchsze
       n=batchsze
       if(((nelms-i)<batchsze).and.&
            &(mod(nelms-i+1,batchsze)/=0))n=mod(nelms,batchsze)
       call ls_mpibcast_realkV_wrapper8(buffer(i:i+n-1),n,master,comm)
    enddo
#endif
  end subroutine ls_mpibcast_realkV_parts84

  subroutine ls_mpibcast_realkV_parts88(buffer,nelms,master,comm,batchsze)
    implicit none
    integer(kind=ls_mpik) :: master
    integer(kind=ls_mpik) :: comm   ! communicator
    integer(kind=8),intent(in) :: batchsze
    integer(kind=8),intent(in) :: nelms
    real(realk) :: buffer(:)
#ifdef VAR_MPI
    integer(kind=8) :: i,n
    do i=1,nelms,batchsze
       n=batchsze
       if(((nelms-i)<batchsze).and.&
            &(mod(nelms-i+1,batchsze)/=0))n=mod(nelms,batchsze)
       call ls_mpibcast_realkV_wrapper8(buffer(i:i+n-1),n,master,comm)
    enddo
#endif
  end subroutine ls_mpibcast_realkV_parts88

end module lsmpi_Bcast


