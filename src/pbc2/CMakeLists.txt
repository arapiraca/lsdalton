add_library(pbc2
  OBJECT
    ${CMAKE_CURRENT_LIST_DIR}/pbc_compare.F90
    ${CMAKE_CURRENT_LIST_DIR}/pbc-matop.F90
    ${CMAKE_CURRENT_LIST_DIR}/pbc-msc.F90
    ${CMAKE_CURRENT_LIST_DIR}/pbc-krsp-op.F90
    ${CMAKE_CURRENT_LIST_DIR}/pbc-nfinteractions.F90
    ${CMAKE_CURRENT_LIST_DIR}/pbc-ffinteractions.F90
    ${CMAKE_CURRENT_LIST_DIR}/pbc-scf.F90
    ${CMAKE_CURRENT_LIST_DIR}/pbc-data.F90
    ${CMAKE_CURRENT_LIST_DIR}/pbc-ffdata.F90
    ${CMAKE_CURRENT_LIST_DIR}/pbcmain.F90
    ${CMAKE_CURRENT_LIST_DIR}/pbc-timings.F90
  )

target_compile_definitions(pbc2
  PRIVATE
    $<$<BOOL:${ENABLE_DEBUGPBC}>:DEBUGPBC>
  )

target_compile_options(pbc2
  PRIVATE
    "${LSDALTON_Fortran_FLAGS};${LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
  )

target_link_libraries(pbc2
  PUBLIC
    lsutil
    LSint
    linears
  )

target_link_libraries(lsdalton
  PUBLIC
    pbc2
  )

