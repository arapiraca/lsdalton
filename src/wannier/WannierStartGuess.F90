module wannier_startguess_module
  use precision
  use wannier_types, only: &
       wannier_config, &
       bravais_lattice
  use wannier_complex_utils
  use lattice_storage
  use memory_handling, only: mem_alloc, mem_dealloc
  use wannier_test_module, only: &
       wtest_orthonormality_wspace, wtest_orthogonalisation_kspace
  use matrix_module, only: matrix
  use matrix_operations!
  use wlattice_domains, only: ltdom_init, ltdom_free, ltdom_getdomain, &
       & lattice_domains_type
  use wannier_utils, only: &
       w_set_kmesh
  implicit none
  private
  
  public :: wannier_orthogonalisation, &
       wannier_kspace_lowdin, wannier_normalization

  
  !type :: bloch_arrays
  !  complex(complexk), pointer, dimension(:,:) :: bloch_array
  !end type bloch_arrays 

contains

  
  !> @brief Orthogonalisation of the Wannier starting guess MO coefficients
  !> @author E. Rebolini, A. S. Hansen, K. R. Leikanger
  !> @date Jan. 2016
  !
  !> @param[inout] mos Starting guess MO coefficients in real space
  !> @param[inout] wconf Wannier configuration settings
  !> @param[inout] smat Overlap matrix in real space
  !> @param[in] nbast Number of basis functions
  subroutine wannier_orthogonalisation(mos,wconf,smat, nbast, lupri, luerr)

    type(lmatrixp), intent(inout)       :: mos, smat
    type(wannier_config), intent(inout) :: wconf
    integer,intent(in)                  :: nbast, lupri, luerr
    
    real(realk), pointer                      :: kpoint_array(:, :)
    integer                                   :: n_bvk(3), nkpoint, i, j
    real(realk)                               :: killthr
    type(bloch_arrays), dimension(:), pointer :: bloch_mos
    
    write(lupri,*) '*** Orthogonalisation of the molecular orbitals ***'

    n_bvk = wconf%n_bvk
        
    if (n_bvk(1) == -1) then
       call lsquit('Error: n_bvk not set, needed in  sr:wannier_orthogonalisation', -1)
    endif
        
    nkpoint = 1
    do i = 1, wconf%blat%dims
       nkpoint = nkpoint * n_bvk(i)
    enddo

    call w_set_kmesh(kpoint_array, nkpoint, wconf%blat, n_bvk)  

    !initialize bloch_arrays "manually" (type not supported in mat_alloc)
    !each element of bloch_mos is a pointer to the Fourier transform of the
    !MOs at a given k-point
    allocate(bloch_mos(nkpoint))
    do i = 1, nkpoint
       allocate(bloch_mos(i)%bloch_array(mos%nrow, mos%ncol))
    enddo

    do i = 1, nkpoint
       if (wconf%debug_level >= 4) then
          write(luerr,'(A,3(3X,F8.4))') '== k-point',kpoint_array(:, i)
       endif
       call bloch_transform_mos(mos, bloch_mos(i)%bloch_array, &
            & kpoint_array(:, i), nkpoint, nbast, wconf, lupri, luerr)

       call wannier_kspace_lowdin(bloch_mos(i)%bloch_array, nbast, mos%ncol, smat, &
            & kpoint_array(:,i), nkpoint, wconf, lupri, luerr )
    enddo
      
    !back transform performed outside the loop
    call wannier_transform_mos(mos, bloch_mos, kpoint_array, nkpoint, nbast, wconf) 

    call wannier_normalization(mos, smat, nbast, wconf, lupri, luerr)
    
    
    !deallocate bloch_arrays
    do i = 1, nkpoint
       deallocate(bloch_mos(i)%bloch_array)
    enddo
    deallocate(bloch_mos)
    
    call mem_dealloc(kpoint_array)
    
    if (wconf%debug_level >= 0) then
       killthr=1.e-8_realk ! stop thr for orthonorm. check
       call wtest_orthonormality_wspace(mos,smat,nbast,wconf,lupri,luerr,killthr)
    endif
       
  end subroutine wannier_orthogonalisation

  !> @brief Lowdin orthogonalisation in k-space
  !> Return the orthogonalised MO at a given k-point
  !> @author Elisa Rebolini
  !> @date Jan. 2016
  !> @param[inout] mos_k MO at a given k-point
  !> @param[in] nbast Number of basis functions
  !> @param[in] smat Overlap matrix in real space
  !> @param[in] kpoint Coordinates of the k-point
  !> @param[in] nkpoint Number of k-points 
  !> @param[inout] wconf Configurtion settings for Wannier module
  subroutine wannier_kspace_lowdin(mos_k,nbast,norb,smat, kpoint, nkpoint, wconf, lupri, luerr)
    integer, intent(in)                       :: nbast, norb, lupri, luerr
    complex(complexk), pointer, intent(inout) :: mos_k(:,:)
    type(wannier_config), intent(inout)       :: wconf
    type(lmatrixp), intent(in)                :: smat
    real(realk), intent(in)                   :: kpoint(3)
    integer, intent(in)                       :: nkpoint

    complex(complexk), parameter :: c1 = cmplx(1.0), c0 = cmplx(0.0)
    complex(complexk) :: norm, sum
         
    complex(complexk), pointer, dimension(:,:)  :: smat_k, smat_k_inv, smat_k_pq
    complex(complexk), pointer, dimension(:,:)  :: tmp, tmp2, inv_sqrt_smat_k
    integer                                     :: i, j, info
    real(realk)                                 :: norm_const

    norm = cmplx(sqrt(real(nkpoint, realk)))
    norm_const = cmplx(1._realk / sqrt(real(nkpoint, realk)))

    
    !Initialization
    call mem_alloc(smat_k, nbast, nbast) !Overlap matrix in k-space AO
    call mem_alloc(smat_k_inv, norb, norb) !Invert overlap matrix in k-space MO
    call mem_alloc(inv_sqrt_smat_k,norb,norb) !Square root of the inverse overlap matrix in k-space MO
    smat_k(:,:) = 0.0_complexk
    smat_k_inv(:,:) = 0.0_complexk
    inv_sqrt_smat_k(:,:) = 0.0_complexk
    
    !Bloch transform of the overlap matrix S_mu,nu -> S(k)_mu,nu 
    call bloch_transform(smat,smat_k,kpoint, nkpoint, nbast, wconf, lupri, luerr)
        
    !S^k_mu nu -> S^k_p q
    call mem_alloc(tmp,norb,nbast)
    tmp(:,:) = 0.0_complexk
    call mem_alloc(smat_k_pq,norb,norb)
    smat_k_pq(:,:) = 0.0_complexk
    call zgemm('c','n',norb,nbast,nbast,c1,mos_k,nbast,smat_k,nbast,&
         & c0,tmp,norb)
    call zgemm('n','n',norb,norb,nbast,c1, &
         & tmp,norb,mos_k,nbast,&
         & c0,smat_k_pq,norb)
    call mem_dealloc(tmp)

    !Invert overlap matrix
    !call wannier_invert(smat_k_pq, smat_k_inv, norb, wconf)
    call wannier_invert_svd(smat_k_pq, smat_k_inv, norb, wconf, lupri, luerr)
        
    !Compute the square root
    call wannier_sqrt(smat_k_inv,inv_sqrt_smat_k,norb,wconf,lupri,luerr)
            
    !Orthogonalise the mos 
    call mem_alloc(tmp,nbast,norb)
    tmp(:,:) = 0.0_complexk
    call zgemm('n','n',nbast,norb,norb,c1,mos_k,nbast,inv_sqrt_smat_k,norb,&
         & c0,tmp,nbast) !transpose?
    mos_k(:,:) = tmp(:,:)
    call mem_dealloc(tmp)

    !Test Orthogonalisation
    if (wconf%debug_level >= 0) then
       call wtest_orthogonalisation_kspace(mos_k,smat_k,norb,nbast,wconf,lupri,luerr)
       write (luerr,'(A,3(2X,F10.4))') 'Test orthonormality at k-point',kpoint 
    endif

    !Free memory
    call mem_dealloc(smat_k)
    call mem_dealloc(smat_k_pq)
    call mem_dealloc(smat_k_inv)
    call mem_dealloc(inv_sqrt_smat_k)

  end subroutine wannier_kspace_lowdin

!> @brief Normalize the transformed mos
!> @author Audun Skau Hansen
!> @date January 2016
!> @param[inout] mos MOs 
!> @param[in] nbast Number of basis functions
!> @param[in] smat Overlap matrix in real space
!> @param[inout] wconf Configurtion settings for Wannier module
  subroutine wannier_normalization(mos,smat,nbast,wconf,lupri,luerr)
        
    type(lmatrixp), intent(inout)       :: mos
    type(lmatrixp), intent(in)          :: smat
    type(wannier_config), intent(inout) :: wconf
    integer,intent(in)                  :: nbast,lupri,luerr

    type(lattice_domains_type) :: dom
    type(matrix) :: c_n,c_m, s_nm,tmp,mat_test,mat_id
    type(matrix), pointer :: c_n_ptr
    logical :: lstat_n, lstat_m, lstat_s
    integer :: n, m, indx_n, indx_m, i,j
    integer :: l1, l2, indx_l1, indx_l2
    integer :: indx_ml1, indx_ml2
    integer :: norb

    norb = mos%ncol
    
    !Initailization of the domain d0
    call ltdom_init(dom, wconf%blat%dims, maxval(mos%cutoffs))
    call ltdom_getdomain(dom, mos%cutoffs, incl_redundant=.true.)

    !Allocation of the lattice matrices
    call mat_init(c_n, nbast, norb)
    call mat_init(c_m, nbast, norb)
    call mat_init(s_nm, nbast, nbast)
    call mat_init(tmp, norb, nbast)
    call mat_init(mat_test, norb, norb)
    call mat_zero(mat_test)

    !calculate inner product of wannier funcs.
    do n = 1, dom%nelms
      do m = 1, dom%nelms
        indx_n = dom%indcs(n)
        indx_m = dom%indcs(m)
        
        call mat_zero(c_n) 
        call mat_zero(c_m)

        call lts_copy(mos,-indx_n,lstat_n,c_n)
        call lts_copy(mos,-indx_m,lstat_m,c_m)
        
        call mat_zero(s_nm)
        call lts_copy(smat,indx_m-indx_n,lstat_s,s_nm)
     
        if (.not. (lstat_M .and. lstat_N .and. lstat_S)) cycle

        call mat_mul(c_n, s_nm,'T','N',1.0_realk,0.0_realk,tmp)
        call mat_mul(tmp,c_m,'N','N',1.0_realk,1.0_realk,mat_test)
        
      enddo
    enddo

    if (wconf%debug_level > 5) then
       call mat_init(mat_id,norb,norb)
       write (luerr,*) ''
       write (luerr,*) "Deviation from normalization in Wannier space"
       write (luerr,*) '   sum_MN C_M^T S_M,N C_N - Id'

       write (lupri,*) ''
       write (lupri,*) "Deviation from normalization in Wannier space"
       write (lupri,*) '   sum_MN C_M^T S_M,N C_N - Id'
       
       call mat_identity(mat_id)
       call mat_daxpy(-1.0_realk,mat_test,mat_id)

       call mat_print(mat_id,1,norb,1,norb,luerr)
       write (luerr,*) ''

       call mat_print(mat_id,1,norb,1,norb,lupri)
       write (lupri,*) ''
       
       call mat_free(mat_id)
       write (luerr,*) "Renormalization of the coefficients in Wannier space"
       write (luerr,*) ''
       
    endif
    
    !normalize the coefficients
    do n = 1, dom%nelms
      indx_n = dom%indcs(n)

      c_n_ptr => lts_get(mos, indx_n)
      if (.not. associated(c_n_ptr)) cycle

      do j=1, norb
         do i=1, nbast
            c_n_ptr%elms((j-1)*nbast + i) = & 
          & c_n_ptr%elms((j-1)*nbast + i)/sqrt(mat_test%elms((j-1)*norb + j))
        enddo
      enddo
    enddo
    
    !Free the lattice matrices
    call mat_free(c_n)
    call mat_free(c_m)
    call mat_free(s_nm)
    call mat_free(tmp)
    call mat_free(mat_Test)
    
    !Free the domain
    call ltdom_free(dom)

  end subroutine wannier_normalization

  subroutine print_complex_mat(mat,nrow,ncol,lunit)
    complex(complexk), pointer, dimension(:,:) :: mat
    integer :: nrow,ncol,lunit

    integer :: i,j
!!$    CHARACTER(LEN=3),DIMENSION(nrow,ncol) :: imag_unit
!!$
!!$    imag_unit= '+i*'
!!$
!!$    WHERE(AIMAG(Mat)<0.)imag_unit = '-i*'

    do i=1,nrow
       write(lunit,'(100(g12.5,a,g12.5,2x))') ( REAL(mat(i,j)),'+i*',&
            AIMAG(mat(i,j)), j=1,ncol )
    enddo
    
  end subroutine print_complex_mat
  
end module wannier_startguess_module
