     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
   This is an MPI run using       3 processes.
 
     Who compiled             | tkjaergaard
     Host                     | localhost.localdomain
     System                   | Linux-4.7.6-200.fc24.x86_64
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | ON
     Fortran compiler         | /opt/intel/compilers_and_libraries_2017.0.098/linu
                              | x/mpi/intel64/bin/mpif90
     Fortran compiler version | GNU Fortran (GCC) 6.2.1 20160916 (Red Hat 6.2.1-2)
     C compiler               | /opt/intel/compilers_and_libraries_2017.0.098/linu
                              | x/mpi/intel64/bin/mpicc
     C compiler version       | gcc (GCC) 6.2.1 20160916 (Red Hat 6.2.1-2)
     C++ compiler             | /usr/lib64/ccache/g++
     C++ compiler version     | g++ (GCC) 6.2.1 20160916 (Red Hat 6.2.1-2)
     BLAS                     | -Wl,--start-group;/opt/intel/mkl/lib/intel64/libmk
                              | l_gf_lp64.so;/opt/intel/mkl/lib/intel64/libmkl_gnu
                              | _thread.so;/opt/intel/mkl/lib/intel64/libmkl_core.
                              | so;/usr/lib64/libpthread.so;/usr/lib64/libm.so;-fo
                              | penmp;-Wl,--end-group
     LAPACK                   | -Wl,--start-group;/opt/intel/mkl/lib/intel64/libmk
                              | l_lapack95_lp64.a;/opt/intel/mkl/lib/intel64/libmk
                              | l_gf_lp64.so;-fopenmp;-Wl,--end-group
     Static linking           | OFF
     Last Git revision        | d88161126d9b287f2e2611b11ffb78a79c7276da
     Git branch               | YangMerge
     Configuration time       | 2016-11-24 11:26:20.376303
  

         Start simulation
     Date and time (Linux)  : Thu Nov 24 12:33:52 2016
     Host name              : localhost.localdomain                   
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    ATOMBASIS                               
                                            
                                            
    Atomtypes=2 Nosymmetry Angstrom                                                                                         
    Charge=3.0 Atoms=1 Basis=STO-3G Aux=6-31G**                                                                             
    N   0.257  -0.363   0.000                                                                                               
    Charge=1.0 Atoms=1 Basis=STO-3G Aux=6-31G**                                                                             
    H   0.257   0.727   0.000                                                                                               
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **INTEGRALS
    .ForceRIMP2memReduced
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .ARH DAVID
    .CONVDYN
    TIGHT
    **INFO
    .DEBUG_MPI_MEM
    **CC
    .CANONICAL
    .RIMP2
    .RIMP2FORCEPDMCALPHA
    .NAF
    .NAFTHRESHOLD
    1.d-4
    .MEMORY
    1.0
    .DECPRINT
    3
    *END OF INPUT
 
 
                      
    Atoms and basis sets
      Total number of atoms        :      2
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 N      3.000 STO-3G                    15        5 [6s3p|2s1p]                                  
          2 H      1.000 STO-3G                     3        1 [3s|1s]                                      
    ---------------------------------------------------------------------
    total          4                               18        6
    ---------------------------------------------------------------------
                      
                      
    Atoms and basis sets
      Total number of atoms        :      2
      THE  AUXILIARY is on R =   2
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 N      3.000 6-31G**                   27       14 [10s4p1d|3s2p1d]                             
          2 H      1.000 6-31G**                    7        5 [4s1p|2s1p]                                  
    ---------------------------------------------------------------------
    total          4                               34       19
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :        6
      Auxiliary basisfunctions           :       19
      Primitive Regular basisfunctions   :       18
      Primitive Auxiliary basisfunctions :       34
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is an OpenMP calculation using   4 threads.
    This is an MPI calculation using   3 processes

    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          

    Maximum size of Fock/density queue in averaging:   10
 
    Due to the tightend SCF convergence threshold we also tighten the integral Threshold
    with a factor:      0.100000

    Dynamic convergence threshold for gradient:   0.20E-05
     
    We perform the calculation in the Grand Canonical basis
    (see PCCP 2009, 11, 5805-5813)
    To use the standard input basis use .NOGCBASIS
 
    Due to the presence of the keyword (default for correlation)
    .NOGCINTEGRALTRANSFORM
    We transform the input basis to the Grand Canonical
    basis and perform integral evaluation using this basis
     
    The Overall Screening threshold is set to              :  1.0000E-09
    The Screening threshold used for Coulomb               :  1.0000E-11
    The Screening threshold used for Exchange              :  1.0000E-09
    The Screening threshold used for One-electron operators:  1.0000E-16
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis

    End of configuration!

 
    Matrix type: mtype_dense
 
    A set of atomic calculations are performed in order to construct
    the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
    as well as JCTC 5, 1027 (2009)
    This is done in order to use the TRILEVEL starting guess and 
    perform orbital localization
    This is Level 1 of the TRILEVEL starting guess and is performed per default.
    The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
    under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
    or orbital localization.
 

    Level 1 atomic calculation on STO-3G Charge   3
    ================================================
  
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift  AO gradient ###
    ******************************************************************************** ###
      1     -7.2386855826    0.00000000000    0.00      0.00000    0.00    3.172E-01 ###
      2     -7.2449023172   -0.00621673460    0.00      0.00000    0.00    2.197E-02 ###
      3     -7.2449319818   -0.00002966462   -1.00      0.00000    0.00    5.269E-05 ###

    Level 1 atomic calculation on STO-3G Charge   1
    ================================================
  
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift  AO gradient ###
    ******************************************************************************** ###
      1     -0.2729303720    0.00000000000    0.00      0.00000    0.00    0.000E+00 ###
 
    Matrix type: mtype_dense

    First density: Atoms in molecule guess

    Iteration 0 energy:       -7.626473092899
 
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  2.00000000E-06
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1     -7.7965905053    0.00000000000    0.00      0.00000    0.00    6.901E-02 ###
      2     -7.8037749673   -0.00718446201    0.00      0.00000   -0.00    1.894E-02 ###
      3     -7.8052451954   -0.00147022812    0.00      0.00000   -0.00    4.502E-03 ###
      4     -7.8053365453   -0.00009134990    0.00      0.00000   -0.00    1.279E-04 ###
      5     -7.8053365719   -0.00000002660    0.00      0.00000   -0.00    1.489E-05 ###
      6     -7.8053365720   -0.00000000010    0.00      0.00000   -0.00    1.718E-07 ###
    SCF converged in      6 iterations
    >>>  CPU Time used in SCF iterations is   0.05 seconds
    >>> wall Time used in SCF iterations is   0.13 seconds

    Total no. of matmuls in SCF optimization:        325

    Number of occupied orbitals:       2
    Number of virtual orbitals:        4

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     2 iterations!

    Calculation of virtual orbital energies converged in     2 iterations!

     E(LUMO):                         0.078464 au
    -E(HOMO):                        -0.316332 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.394796 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.00718446201    0.0000   -0.0000    0.0000000
      3   -0.00147022812    0.0000   -0.0000    0.0000000
      4   -0.00009134990    0.0000   -0.0000    0.0000000
      5   -0.00000002660    0.0000   -0.0000    0.0000000
      6   -0.00000000010    0.0000   -0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1            -7.79659050525988028824      0.690094146311690D-01
        2            -7.80377496727041908287      0.189362340970877D-01
        3            -7.80524519539514116673      0.450243806974475D-02
        4            -7.80533654529279807832      0.127878570602698D-03
        5            -7.80533657189334384441      0.148943989692550D-04
        6            -7.80533657199815955607      0.171814653933716D-06

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                        -7.805336571998
          Nuclear repulsion:                       1.456451032018
          Electronic energy:                      -9.261787604017



    -- Full molecular info --

    FULL: Overall charge of molecule    :      0

    FULL: Number of electrons           :      4
    FULL: Number of atoms               :      2
    FULL: Number of basis func.         :      6
    FULL: Number of aux. basis func.    :     19
    FULL: Number of core orbitals       :      1
    FULL: Number of valence orbitals    :      1
    FULL: Number of occ. orbitals       :      2
    FULL: Number of occ. alpha orbitals : ******
    FULL: Number of occ. beta  orbitals :  32581
    FULL: Number of virt. orbitals      :      4
    FULL: Local memory use type full    :  0.69E-06
    FULL: Distribute matrices           : F
    FULL: Using frozen-core approx.     : F

 Allocate space for molecule%Co on Master use_bg= F
 Allocate space for molecule%Cv on Master use_bg= F
    Memory set in input to be:    1.000     GB


    =============================================================================
         -- Full molecular Coupled-Cluster calculation -- 
    =============================================================================

    Using canonical orbitals as requested in input!



 ================================================ 
              Full molecular driver               
 ================================================ 

 MemoryReduced RIMP2 scheme chosen as default
 Use Natural Auxiliary Functions (NAF) with Threshold =    1.0000000000000000E-004
3 center RI: Allocating MO (alpha|AI)      0.00000 GB
3 center RI: Allocating 5dim Buffer      0.00000 GB for each thread =       0.00001 GB
3 center RI: Allocating TmpArray of       0.00000 GB
 NAF: Auxiliary functions         =           19
 NAF: Natural Auxiliary functions =            7
 RIMP2 CORRELATION ENERGY =   -9.7903877549674004E-003
    >>>  CPU Time used in FULL RIMP2 is   0.01 seconds
    >>> wall Time used in FULL RIMP2 is   0.01 seconds
 Overall Time spent in MPI Communication and MPI Wait for rank=           0
 >>>  WALL Time used MPI Communication inc. some Wait   0.00 seconds
 >>>  CPU Time used MPI Communication inc. some Wait   0.00 seconds
 >>>  WALL Time used in MPI Wait   0.00 seconds
 >>>  CPU Time used in MPI Wait   0.00 seconds
  




    ******************************************************************************
    *                             CC ENERGY SUMMARY                              *
    ******************************************************************************

     E: Hartree-Fock energy                            :       -7.8053365720
     E: Correlation energy                             :       -0.0097903878
     E: Total RIMP2 energy                             :       -7.8151269598



    CC Memory summary
    -----------------
     Allocated memory for array4   :   0.000     GB
     Memory in use for array4      :   0.000     GB
     Max memory in use for array4  :   0.000     GB
    ------------------


    ------------------------------------------------------
    Total CPU  time used in CC           :        0.144100E-01 s
    Total Wall time used in CC           :        0.300000E-01 s
    ------------------------------------------------------


    Hostname       : localhost.localdomain                             
    Job finished   : Date: 24/11/2016   Time: 12:33:52



    =============================================================================
                              -- end of CC program --
    =============================================================================



    Total no. of matmuls used:                       353
    Total no. of Fock/KS matrix evaluations:           7
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                          1.568 MB
      Max allocated memory, type(matrix)                  31.184 kB
      Max allocated memory, real(realk)                    1.450 MB
      Max allocated memory, integer                       16.057 kB
      Max allocated memory, logical                        4.704 kB
      Max allocated memory, character                    146.560 kB
      Max allocated memory, AOBATCH                       16.896 kB
      Max allocated memory, ODBATCH                        1.320 kB
      Max allocated memory, LSAOTENSOR                     2.496 kB
      Max allocated memory, SLSAOTENSOR                    2.944 kB
      Max allocated memory, ATOMTYPEITEM                 229.584 kB
      Max allocated memory, ATOMITEM                       1.584 kB
      Max allocated memory, LSMATRIX                       3.472 kB
      Max allocated memory, OverlapT                     109.056 kB
      Max allocated memory, linkshell                      0.252 kB
      Max allocated memory, integrand                    199.584 kB
      Max allocated memory, integralitem                   1.037 MB
      Max allocated memory, IntWork                       96.192 kB
      Max allocated memory, Overlap                       37.808 kB
      Max allocated memory, ODitem                         0.960 kB
      Max allocated memory, LStensor                       4.863 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    The total memory used across all MPI nodes
      Max allocated memory, TOTAL                         16.941 MB
    time spent in unlock:        0.0000000000
    Largest memory allocated on a single MPI slave node (including Master)
      Max allocated memory, TOTAL                          9.564 MB
    Largest memory allocated on a single MPI slave node (exclusing Master)
      Max allocated memory, TOTAL                          9.564 MB
      Memory statistics for MPI node number         1 
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated MPI memory (TOTAL):                   0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (type(matrix)):            0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (real):                    0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (MPI):                     0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (complex):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (integer):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (logical):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (character):               0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (AOBATCH):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ODBATCH):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (LSAOTENSOR):              0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (SLSAOTENSOR):             0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (GLOBALLSAOTENSOR):        0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ATOMTYPEITEM):            0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ATOMITEM):                0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (LSMATRIX):                0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (DECORBITAL):              0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (DECFRAG):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (overlapType):             0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (BATCHTOORB):              0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (DECAOBATCHINFO):          0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (MYPOINTER):               0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (fragmentAOS):             0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ARRAY2):                  0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ARRAY4):                  0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ARRAY):                   0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (PNOSpaceInfo):            0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (MP2DENS):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (TRACEBACK):               0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (MP2GRAD):                 0 byte  - Should be zero - otherwise a leakage is present
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Additional Memory information          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated MPI memory (linkshell):               0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (integrand):               0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (integralitem):            0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (IntWork):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (overlap):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ODitem):                  0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (lstensor):                0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (FMM   ):                  0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (XC    ):                  0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (lvec_data):               0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (lattice_cell):            0 byte  - Should be zero - otherwise a leakage is present
      Max allocated memory, TOTAL                          9.564 MB
      Max allocated memory, type(matrix)                   2.280 kB
      Max allocated memory, real(realk)                    9.414 MB
      Max allocated memory, integer                      149.494 kB
      Max allocated memory, logical                        2.604 kB
      Max allocated memory, character                     56.368 kB
      Max allocated memory, AOBATCH                       29.184 kB
      Max allocated memory, ODBATCH                        1.584 kB
      Max allocated memory, LSAOTENSOR                     4.368 kB
      Max allocated memory, SLSAOTENSOR                    4.048 kB
      Max allocated memory, ATOMTYPEITEM                   2.066 MB
      Max allocated memory, ATOMITEM                       4.752 kB
      Max allocated memory, LSMATRIX                       5.600 kB
      Max allocated memory, OverlapType                  109.056 kB
      Max allocated memory, linkshell                      0.252 kB
      Max allocated memory, integrand                      1.032 MB
      Max allocated memory, integralitem                   1.843 MB
      Max allocated memory, Overlap                        6.205 MB
      Max allocated memory, ODitem                         1.152 kB
      Max allocated memory, LStensor                       9.144 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

      Memory statistics for MPI node number         2 
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated MPI memory (TOTAL):                   0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (type(matrix)):            0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (real):                    0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (MPI):                     0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (complex):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (integer):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (logical):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (character):               0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (AOBATCH):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ODBATCH):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (LSAOTENSOR):              0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (SLSAOTENSOR):             0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (GLOBALLSAOTENSOR):        0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ATOMTYPEITEM):            0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ATOMITEM):                0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (LSMATRIX):                0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (DECORBITAL):              0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (DECFRAG):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (overlapType):             0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (BATCHTOORB):              0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (DECAOBATCHINFO):          0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (MYPOINTER):               0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (fragmentAOS):             0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ARRAY2):                  0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ARRAY4):                  0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ARRAY):                   0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (PNOSpaceInfo):            0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (MP2DENS):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (TRACEBACK):               0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (MP2GRAD):                 0 byte  - Should be zero - otherwise a leakage is present
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Additional Memory information          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated MPI memory (linkshell):               0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (integrand):               0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (integralitem):            0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (IntWork):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (overlap):                 0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (ODitem):                  0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (lstensor):                0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (FMM   ):                  0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (XC    ):                  0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (lvec_data):               0 byte  - Should be zero - otherwise a leakage is present
      Allocated MPI memory (lattice_cell):            0 byte  - Should be zero - otherwise a leakage is present
      Max allocated memory, TOTAL                          5.809 MB
      Max allocated memory, type(matrix)                   2.136 kB
      Max allocated memory, real(realk)                    5.660 MB
      Max allocated memory, integer                      149.476 kB
      Max allocated memory, logical                        2.604 kB
      Max allocated memory, character                     56.368 kB
      Max allocated memory, AOBATCH                       17.408 kB
      Max allocated memory, ODBATCH                        1.320 kB
      Max allocated memory, LSAOTENSOR                     4.368 kB
      Max allocated memory, SLSAOTENSOR                    4.048 kB
      Max allocated memory, ATOMTYPEITEM                   2.066 MB
      Max allocated memory, ATOMITEM                       4.752 kB
      Max allocated memory, LSMATRIX                       3.472 kB
      Max allocated memory, OverlapType                  109.056 kB
      Max allocated memory, linkshell                      0.252 kB
      Max allocated memory, integrand                      1.032 MB
      Max allocated memory, integralitem                   1.843 MB
      Max allocated memory, Overlap                        2.518 MB
      Max allocated memory, ODitem                         0.960 kB
      Max allocated memory, LStensor                       4.991 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

      Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    >>>  CPU Time used in LSDALTON is   0.26 seconds
    >>> wall Time used in LSDALTON is   0.62 seconds

    End simulation
     Date and time (Linux)  : Thu Nov 24 12:33:52 2016
     Host name              : localhost.localdomain                   
