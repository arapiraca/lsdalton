import pytest

from pylsdalton import sample_dal, sample_mol, create, destroy


@pytest.fixture(scope="session")
def pylsd_instance(tmp_path_factory):
    tmp = tmp_path_factory.mktemp(basename="tmp_pylsd")
    dal = sample_dal(tmp)
    mol = sample_mol(tmp)
    create(dalfile=dal, molfile=mol)
    yield dal, mol
    destroy()


@pytest.fixture(scope="session")
def dal(tmp_path_factory):
    tmp = tmp_path_factory.mktemp(basename="tmp_pylsd")
    return sample_dal(tmp)


@pytest.fixture(scope="session")
def mol(tmp_path_factory):
    tmp = tmp_path_factory.mktemp(basename="tmp_pylsd")
    return sample_mol(tmp)
