from pathlib import Path

import numpy as np
import pytest

import pylsdalton as pylsd


def test_dimensions(pylsd_instance):
    natoms = pylsd.number_atoms()
    nbasis = pylsd.number_basis()
    nelectron = pylsd.number_electrons()

    assert nbasis == 6
    assert natoms == 2
    assert nelectron == 4


def test_overlap(shared_datadir, pylsd_instance):
    # Undifferentiated
    overlap = pylsd.overlap_matrix()

    ref_S = np.load(Path(shared_datadir / "overlap.npy").resolve(),)

    assert overlap == pytest.approx(ref_S)

    # Geometrical derivative
    overlap_geo = pylsd.overlap_matrix(geoOrder=2)
    ref_S_geo = np.load(Path(shared_datadir / "overlap_geo.npy").resolve())

    assert overlap_geo == pytest.approx(ref_S_geo)

    # Magnetic derivative
    overlap_mag = pylsd.overlap_matrix(magOrder=1)
    ref_S_mag = np.load(Path(shared_datadir / "overlap_mag.npy").resolve())
    assert overlap_mag == pytest.approx(ref_S_mag)

def test_kinetic(shared_datadir, pylsd_instance):
    kinetic = pylsd.kinetic_energy_matrix()

    ref_T = np.load(Path(shared_datadir / "kinetic.npy").resolve())

    assert kinetic == pytest.approx(ref_T)

    # Geometrical derivative
    kinetic_geo = pylsd.kinetic_energy_matrix(geoOrder=2)
    ref_T_geo = np.load(Path(shared_datadir / "kinetic_geo.npy").resolve())

    assert kinetic_geo == pytest.approx(ref_T_geo)

def test_nucel(shared_datadir, pylsd_instance):

    # Undifferentiated
    nucel = pylsd.nuclear_electron_attraction_matrix()

    ref_VNe = np.load(Path(shared_datadir / "nucel.npy").resolve(),)
    assert nucel == pytest.approx(ref_VNe)

    # Geometrical derivative
    nucel_geo = pylsd.nuclear_electron_attraction_matrix(geoOrder=2)

    ref_VNe_geo = np.load(Path(shared_datadir / "nucel_geo.npy").resolve())
    assert nucel_geo == pytest.approx(ref_VNe_geo)


def test_coulomb(shared_datadir, pylsd_instance):
    density = np.load(Path(shared_datadir / "density.npy").resolve(),)

    # Undifferentiated
    coulomb = pylsd.coulomb_matrix(density)

    ref_J = np.load(Path(shared_datadir / "coulomb.npy").resolve(),)
    assert coulomb == pytest.approx(ref_J)

    # Geometrical derivative
    coulomb_geo = pylsd.coulomb_matrix(density, geoOrder=2)

    ref_J_geo = np.load(Path(shared_datadir / "coulomb_geo.npy").resolve())
    assert coulomb_geo == pytest.approx(ref_J_geo)

    # Magnetic derivative
    coulomb_mag = pylsd.coulomb_matrix(density, magOrder=1)

    ref_J_mag = np.load(Path(shared_datadir / "coulomb_mag.npy").resolve())
    assert coulomb_mag == pytest.approx(ref_J_mag)


def test_exchange(shared_datadir, pylsd_instance):
    density = np.load(Path(shared_datadir / "density.npy").resolve(),)

    # Undifferentiated
    exchange = pylsd.exchange_matrix(density)

    ref_K = np.load(Path(shared_datadir / "exchange.npy").resolve(),)
    assert exchange == pytest.approx(ref_K)

    # Geometrical derivative
    exchange_geo = pylsd.exchange_matrix(density, geoOrder=2)

    ref_K_geo = np.load(Path(shared_datadir / "exchange_geo.npy").resolve())
    assert exchange_geo == pytest.approx(ref_K_geo)

    # Magnetic derivative
    exchange_mag = pylsd.exchange_matrix(density, magOrder=1)

    ref_K_mag = np.load(Path(shared_datadir / "exchange_mag.npy").resolve())
    assert exchange_mag == pytest.approx(ref_K_mag)


def test_XC(shared_datadir, pylsd_instance):
    density = np.load(Path(shared_datadir / "density.npy").resolve(),)

    EXC, XC = pylsd.exchange_correlation_matrix(density, testElectrons=False)

    ref_XC = np.load(Path(shared_datadir / "XC.npy").resolve(),)
    ref_E = np.load(Path(shared_datadir / "EXC.npy").resolve(),)
    assert XC == pytest.approx(ref_XC)
    assert EXC == pytest.approx(ref_E)


def test_Fock(shared_datadir, pylsd_instance):
    density = np.load(Path(shared_datadir / "density.npy").resolve(),)

    fock = pylsd.fock_matrix(density, testElectrons=False)

    ref_F = np.load(Path(shared_datadir / "Fock.npy").resolve(),)
    assert fock == pytest.approx(ref_F)

def test_ep_matrix(shared_datadir, pylsd_instance):
    points = np.load(Path(shared_datadir / "ep_points.npy").resolve())
    moments = np.load(Path(shared_datadir / "ep_moments.npy").resolve())
    ref_ep_mat = np.load(Path(shared_datadir / "ep_matrix.npy").resolve())

    ep_mat = pylsd.electrostatic_potential_matrix(moments, points)

    assert ep_mat == pytest.approx(ref_ep_mat)

def test_ep_vector(shared_datadir, pylsd_instance):
    density = np.load(Path(shared_datadir / "density.npy").resolve())
    points = np.load(Path(shared_datadir / "ep_points.npy").resolve())
    ref_ep_vec = np.load(Path(shared_datadir / "ep_vector.npy").resolve())

    ep_vec = pylsd.electrostatic_potential_vector(density, points)

#   np.save("/Users/simensr/Dalton/2020/test/pylsdalton/data/ep_vector.npy", ep_vec)
    assert ep_vec == pytest.approx(ref_ep_vec)

def test_ep_integrals(shared_datadir, pylsd_instance):
    points = np.load(Path(shared_datadir / "ep_points.npy").resolve())
    ref_ep_int = np.load(Path(shared_datadir / "ep_integrals.npy").resolve())

    ep_int = pylsd.electrostatic_potential_integrals(points)

    assert ep_int == pytest.approx(ref_ep_int)


@pytest.mark.parametrize(
    "specs,ref",
    [("RRRRC", "ERI_4.npy"), ("DERRC", "ERI_3.npy"), ("DEDEC", "ERI_2.npy")],
    ids=["4_index", "3_index", "2_index"],
)
def test_eri(shared_datadir, pylsd_instance, specs, ref):
    eri = pylsd.eri(specs)

    reference = np.load(Path(shared_datadir / ref).resolve())
    assert eri == pytest.approx(reference)

def test_eri4_geo(shared_datadir, pylsd_instance):
    # Geometrical derivative
    eri_geo = pylsd.eri('RRRRC', geoOrder=2)

    ref_eri_geo = np.load(Path(shared_datadir / "ERI_4_geo.npy").resolve())
    assert eri_geo == pytest.approx(ref_eri_geo)
