  
     ******************************************************************    
     **********  LSDALTON - An electronic structure program  **********    
     ******************************************************************    
  
  
      This is output from LSDALTON 2014.0
  
  
      IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
      THE FOLLOWING PAPER SHOULD BE CITED:
      
      K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
      L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
      P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
      J. J. Eriksen, P. Ettenhuber, B. Fernandez,
      L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
      A. Halkier, C. Haettig, H. Heiberg,
      T. Helgaker, A. C. Hennum, H. Hettema,
      E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
      M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
      D. Jonsson, P. Joergensen, J. Kauczor,
      S. Kirpekar, T. Kjaergaard, W. Klopper,
      S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
      A. Krapp, K. Kristensen, A. Ligabue,
      O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
      C. Neiss, C. B. Nielsen, P. Norman,
      J. Olsen, J. M. H. Olsen, A. Osted,
      M. J. Packer, F. Pawlowski, T. B. Pedersen,
      P. F. Provasi, S. Reine, Z. Rinkevicius,
      T. A. Ruden, K. Ruud, V. Rybkin,
      P. Salek, C. C. M. Samson, A. Sanchez de Meras,
      T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
      K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
      P. R. Taylor, A. M. Teale, E. I. Tellgren,
      D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
      O. Vahtras, M. A. Watson, D. J. D. Wilson,
      M. Ziolkowski, and H. AAgren,
      "The Dalton quantum chemistry program system",
      WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                                
      LSDALTON authors in alphabetical order (main contribution(s) in parenthesis)
      ----------------------------------------------------------------------------
      Vebjoern Bakken,         University of Oslo,        Norway    (geometry optimizer)
      Radovan Bast,            KTH Stockholm,             Sweden    (CMake, Testing)
      Sonia Coriani,           University of Trieste,     Italy     (response)
      Patrick Ettenhuber,      Aarhus University,         Denmark   (CCSD)
      Trygve Helgaker,         University of Oslo,        Norway    (supervision)
      Stinne Hoest,            Aarhus University,         Denmark   (SCF optimization)
      Ida-Marie Hoeyvik,       Aarhus University,         Denmark   (orbital localization, SCF optimization)
      Robert Izsak,            University of Oslo,        Norway    (ADMM)
      Branislav Jansik,        Aarhus University,         Denmark   (trilevel, orbital localization)
      Poul Joergensen,         Aarhus University,         Denmark   (supervision)
      Joanna Kauczor,          Aarhus University,         Denmark   (response solver)
      Thomas Kjaergaard,       Aarhus University,         Denmark   (response, integrals)
      Andreas Krapp,           University of Oslo,        Norway    (FMM, dispersion-corrected DFT)
      Kasper Kristensen,       Aarhus University,         Denmark   (response, DEC)
      Patrick Merlot,          University of Oslo,        Norway    (ADMM)
      Cecilie Nygaard,         Aarhus University,         Denmark   (SOEO)
      Jeppe Olsen,             Aarhus University,         Denmark   (supervision)
      Simen Reine,             University of Oslo,        Norway    (integrals, geometry optimizer)
      Vladimir Rybkin,         University of Oslo,        Norway    (geometry optimizer, dynamics)
      Pawel Salek,             KTH Stockholm,             Sweden    (FMM, DFT functionals)
      Andrew M. Teale,         University of Nottingham   England   (E-coefficients)
      Erik Tellgren,           University of Oslo,        Norway    (density fitting, E-coefficients)
      Andreas J. Thorvaldsen,  University of Tromsoe,     Norway    (response)
      Lea Thoegersen,          Aarhus University,         Denmark   (SCF optimization)
      Mark Watson,             University of Oslo,        Norway    (FMM)
      Marcin Ziolkowski,       Aarhus University,         Denmark   (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
 Who compiled             | simensr
 Host                     | 1x-193-157-199-133.uio.no
 System                   | Darwin-14.0.0
 CMake generator          | Unix Makefiles
 Processor                | x86_64
 64-bit integers          | OFF
 MPI                      | OFF
 Fortran compiler         | /usr/local/bin/gfortran
 Fortran compiler version | GNU Fortran (GCC) 5.0.0 20141012 (experimental)
 C compiler               | /usr/local/bin/gcc
 C compiler version       | gcc (GCC) 5.0.0 20141012 (experimental)
 C++ compiler             | /usr/local/bin/g++
 C++ compiler version     | g++ (GCC) 5.0.0 20141012 (experimental)
 BLAS                     | /usr/lib/libblas.dylib
 LAPACK                   | /usr/lib/liblapack.dylib
 Static linking           | OFF
 Last Git revision        | 6912f763cad369377e4f1999ea67e2a2d27473fe
 Git branch               | XCfun-dev
 Configuration time       | 2014-12-09 17:18:56.079323
 The XCfun module is deactivated, PS functional evaluation is used instead

Start simulation
     Date and time (Linux)  : Tue Dec  9 17:30:25 2014
     Host name              : 1x-193-157-199-133.uio.no               
                      
 -----------------------------------------
          PRINTING THE MOLECULE.INP FILE 
 -----------------------------------------
                      
  BASIS                                   
  6-31G                                   
  HeH, CCSD( T)/cc-pVQZ opt. geometry     
  Test open shell response                
  Atomtypes=2 Charge=0 Nosymmetry Angstrom                    
  Charge=1.0 Atoms=1                                          
  H     0.0000000000   0.0000000000   0.0000000000            
  Charge=2.0 Atoms=1                                          
  He    0.0000000000   0.0000000000   3.9218840000            
                      
 -----------------------------------------
          PRINTING THE LSDALTON.INP FILE 
 -----------------------------------------
                      
  **GENERAL                                                                                           
  .PSFUN                                                                                              
  **WAVE FUNCTIONS                                                                                    
  .DFT                                                                                                
  B3LYP                                                                                               
  *DFT INPUT                                                                                          
  .FINE                                                                                               
  .GRID TYPE                                                                                          
   BECKEORIG LMG                                                                                      
  *DENSOPT                                                                                            
  .RH                                                                                                 
  .DIIS                                                                                               
  **INFO                                                                                              
  .DEBUG_MPI_MEM                                                                                      
  *END OF INPUT                                                                                       
  
  MOLPRINT IS SET TO       0
  Coordinates are entered in Angstroms and converted to atomic units.
  Conversion factor : 1 bohr = 0.52917721 A


                    Cartesian Coordinates Linsca (au)
                    ---------------------------------

   Total number of coordinates:  6
   Written in atomic units    
 
   1   H        x      0.0000000000
   2            y      0.0000000000
   3            z      0.0000000000
 
   4   He       x      0.0000000000
   5            y      0.0000000000
   6            z      7.4112866890
 
WARNING:  No bonds - no atom pairs are within normal bonding distances
WARNING:  maybe coordinates were in Bohr, but program were told they were in Angstrom ?
 
 BASISSETLIBRARY  : REGULAR  
   
BASISSETLIBRARY
 Number of Basisset           1
 BASISSET:  6-31G                                             
  CHARGES:    1.0000    2.0000
                      
   
  CALLING BUILD BASIS WITH DEFAULT REGULAR   BASIS
   
 OPENING FILE/Users/simensr/Dalton/2014/xcbuild-omp/basis/6-31G
                      
THE MOLECULE
 --------------------------------------------------------------------
 Molecular Charge                    :    0.0000
    Regular basisfunctions             :        4
    Valence basisfunctions             :        0
    Primitive Regular basisfunctions   :        8
    Primitive Valence basisfunctions   :        0
 --------------------------------------------------------------------
                      
                      
 --------------------------------------------------------------------
  atom  charge  Atomicbasis    Phantom   nPrimREG  nContREG
 --------------------------------------------------------------------
     1   1.000  6-31G           F              4           2
     2   2.000  6-31G           F              4           2
                      
 The cartesian centers in Atomic units. 
  ATOM  NAME  ISOTOPE            X                 Y                 Z     
     1  H           1        0.00000000        0.00000000        0.00000000
     2  He          1        0.00000000        0.00000000        7.41128669
                      
                      
Atoms and basis sets
  Total number of atoms        :      2
  THE  REGULAR   is on R =   1
---------------------------------------------------------------------
  atom label  charge basisset                prim     cont   basis
---------------------------------------------------------------------
      1 H      1.000 6-31G                      4        2 [4s|2s]                                      
      2 He     2.000 6-31G                      4        2 [4s|2s]                                      
---------------------------------------------------------------------
total          3                                8        4
---------------------------------------------------------------------
                      
 Configuration:
 ==============
    This is an OpenMP calculation using   8 threads.
    This is a serial calculation (no MPI)
 
     This is a DFT calculation of type: B3LYP
 Weighted mixed functional:
                       VWN:    0.19000
                       LYP:    0.81000
                     Becke:    0.72000
                    Slater:    0.80000
   
  The Exchange-Correlation Grid specifications:
  Radial Quadrature : LMG scheme
                      Radial grid as proposed by R. Lindh, P.A. Malmqvist
                      and L. Gagliardi. Theor. Chem. Acc. (2001) 106, 178
  Space partitioning: Becke partitioning scheme without atomic size correction
                      J. Chem. Phys. (1988) vol 88 page 2547

  We use grid pruning according to Mol. Phys. (1993) vol 78 page 997

     DFT LSint Radial integration threshold:   0.2154D-16
     DFT LSint integration order range     :   [  5:  47]
     Hardness of the partioning function   :   3
     DFT LSint screening thresholds        :   0.10D-08   0.20D-09   0.20D-11
     Threshold for number of electrons     :   0.10D-02
     The Exact Exchange Factor             : 0.2000D+00

Density subspace min. method    : DIIS                    
Density optimization : Diagonalization                    

 Maximum size of Fock/density queue in averaging:           7

 WARNING WARNING WARNING spin check commented out!!! /Stinne
 Unrestricted calculations require the use of XCFUN.
 Remove keyword .PSFUN under **GENERAL to run unrestriced calculations.

  --- SEVERE ERROR, PROGRAM WILL BE ABORTED ---
     Date and time (Linux)  : Tue Dec  9 17:30:25 2014
     Host name              : 1x-193-157-199-133.uio.no               

 Reason: Unrestricted calculations require the use of XCFUN

 >>>> Total CPU  time used in LSDALTON:   0.03 seconds
 >>>> Total wall time used in LSDALTON:   0.03 seconds
