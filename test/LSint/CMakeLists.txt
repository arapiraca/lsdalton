add_lsdalton_runtest_v1(
  NAME
    LSDALTON_MBIE
  )
add_lsdalton_runtest_v1(
  NAME
    LSDALTON_QQR
  )
add_lsdalton_runtest_v1(
  NAME
  LSDALTON_segmented
  )
add_lsdalton_runtest(
  NAME
  LSDALTON_dfK
  )
add_lsdalton_runtest_v1(
  NAME
  LSDALTON_FMM
  )
add_lsdalton_runtest_v1(NAME LSDALTON_Link)
add_lsdalton_runtest_v1(
  NAME
  LSDALTON_ethane_pari_b3lyp
  )
add_lsdalton_runtest_v1(
  NAME
  LSDALTON_ethane_pari_b3lyp_unconstrained
  )
add_lsdalton_runtest_v1(
  NAME
  LSDALTON_ethane_pari_b3lyp_ChgConst
  )
add_lsdalton_runtest_v1(
  NAME
  LSDALTON_ethane_pari_b3lyp_ChgDipConst
  )
add_lsdalton_runtest_v1(
  NAME
  LSDALTON_moparik
  )
add_lsdalton_runtest_v1(
  NAME
  LSDALTON_hodi
  LABELS
  "hodi"
  )
add_lsdalton_test(NAME LSDALTON_b3lyp_water        )
add_lsdalton_test(NAME LSDALTON_userdefined_basis1 )
add_lsdalton_test(NAME LSDALTON_userdefined_basis2 )
add_lsdalton_test(NAME LSDALTON_NOBQBQ2            )
add_lsdalton_test(NAME LSDALTON_pointcharge        )
add_lsdalton_test(NAME LSDALTON_pointcharge2       )
add_lsdalton_test(NAME LSDALTON_phantom            )
add_lsdalton_test(NAME LSDALTON_phantom2           )
add_lsdalton_test(NAME LSDALTON_EP                 )
add_lsdalton_test(NAME LSDALTON_PropertyTest1      )
add_lsdalton_test(NAME LSDALTON_contang            )
add_lsdalton_test(NAME LSDALTON_magderiv           )
add_lsdalton_test(NAME LSDALTON_magderiv2          )
add_lsdalton_test(NAME LSDALTON_magderiv3          )
add_lsdalton_test(NAME LSDALTON_camb3lyp           )
add_lsdalton_test(NAME LSDALTON_camb3lyp2          )
add_lsdalton_test(NAME LSDALTON_screen1            )
add_lsdalton_test(NAME LSDALTON_screen2            )
add_lsdalton_test(NAME LSDALTON_cartesian_ecoeff   )
add_lsdalton_test(NAME LSDALTON_DaLink             )
add_lsdalton_test(NAME LSDALTON_DaLink2            )
add_lsdalton_test(NAME LSDALTON_DENSFIT_FMM        )
add_lsdalton_test(NAME LSDALTON_dftfunc_bp86          )
add_lsdalton_test(NAME LSDALTON_dftfunc_bpw91         )
add_lsdalton_test(NAME LSDALTON_dftfunc_kt3           )
add_lsdalton_test(NAME LSDALTON_dftfunc_lb94          )
add_lsdalton_test(NAME LSDALTON_dftfunc_psfun_pbe0    )
add_lsdalton_test(NAME LSDALTON_dftfunc_pw86x         )
add_lsdalton_test(NAME LSDALTON_dftfunc_vwn3          )
add_lsdalton_test(NAME LSDALTON_ethane_df_lda_jengine )
add_lsdalton_test(NAME LSDALTON_ethane_df_overlap_lda )
add_lsdalton_test(NAME LSDALTON_ethane_lda_jengine    )
add_lsdalton_test(NAME LSDALTON_FMM_trilevel          )
# TODO Uncomment or remove
#add_lsdalton_test(NAME LSDALTON_fmm_gradient       )
#add_lsdalton_test(NAME LSDALTON_fmm_df_gradient    )
#add_lsdalton_test(NAME LSDALTON_fmm_twoel_gradient )
add_lsdalton_test(NAME LSDALTON_INCREM               )
add_lsdalton_test(NAME LSDALTON_INCREM2              )
add_lsdalton_test(NAME LSDALTON_J_NSETUV             )
add_lsdalton_test(NAME LSDALTON_NOFAMILY             )
add_lsdalton_test(NAME LSDALTON_pass                 )
add_lsdalton_test(NAME LSDALTON_PrimAng              )
add_lsdalton_test(NAME LSDALTON_ethane_dfK_dft       )
add_lsdalton_test(NAME LSDALTON_UNCONT                          )
add_lsdalton_test(NAME LSDALTON_HF_cartesian                    )
add_lsdalton_test(NAME LSDALTON_augTZ                           )
add_lsdalton_test(NAME LSDALTON_donotsavegab                    )
add_lsdalton_test(NAME LSDALTON_INCREMDALINK                    )
add_lsdalton_test(NAME LSDALTON_SR_EXCHANGE                     )
add_lsdalton_test(NAME LSDALTON_LSlib                           )
add_lsdalton_test(NAME LSDALTON_UHF_DEFAULT                     )
add_lsdalton_test(NAME LSDALTON_UHF_hessian                     )
add_lsdalton_test(NAME LSDALTON_UHF_JENGINE                     )
add_lsdalton_test(NAME LSDALTON_UHF_JENGINE_LINK LABELS "essential;ContinuousIntegration")
add_lsdalton_test(NAME LSDALTON_UHF_LINK                        )
add_lsdalton_test(NAME LSDALTON_uncontAObatch            )

# Conditionally available tests
if(NOT ENABLE_MPI)
  add_lsdalton_test(NAME LSDALTON_geoderiv         )
  add_lsdalton_runtest_v1(
    NAME
      ThermiteTensorFamework
    LABELS
      "hodi"
    )
  add_lsdalton_runtest_v1(
    NAME
      LSDALTON_hf_nrparijk_neonatom
    )
  add_lsdalton_runtest_v1(
    NAME
      LSDALTON_hf_nrparijk_ethane
    )
endif()

if(ENABLE_XCFUN)
  add_lsdalton_runtest(
    NAME
      LSDALTON_dftfunc_GGAKey
    LABELS
      "ContinuousIntegration"
    )
  add_lsdalton_runtest(
    NAME
      LSDALTON_UDFT
    )
  add_lsdalton_runtest(
    NAME
      LSDALTON_admm_df_unres
    )
  if(ENABLE_RSP)
    add_lsdalton_runtest(
      NAME
        LSDALTON_df_admm_beta_cam100_xcfun
      LABELS
        "ContinuousIntegration"
      COST
        200
      )
  else()
    add_lsdalton_runtest(
      NAME
        LSDALTON_df_admm_alpha_cam100_xcfun
      LABELS
        "ContinuousIntegration"
      )
  endif()
else()
  add_lsdalton_test(NAME LSDALTON_dftfunc_GGAKey_psfun )
  add_lsdalton_runtest_v1(
    NAME
      LSDALTON_UDFT_psfun
    )
endif()

if(ENABLE_RSP)
  add_lsdalton_runtest_v1(
    NAME
      LSDALTON_df_admm_beta_cam100_psfun
    LABELS
      "ContinuousIntegration"
    )
  add_lsdalton_test(NAME LSDALTON_ethane_df_dft_molgrad LABELS "lsresponse")
  add_lsdalton_test(NAME LSDALTON_ethane_dft_molgrad    LABELS "lsresponse")
  add_lsdalton_test(NAME LSDALTON_ethane_dft_disp       LABELS "lsresponse")
  add_lsdalton_test(NAME LSDALTON_ethane_dft_D2disp     LABELS "lsresponse")
  add_lsdalton_test(NAME LSDALTON_ethane_dft_D3disp     LABELS "lsresponse")
  add_lsdalton_test(NAME LSDALTON_ethane_dft_D3bjdisp   LABELS "lsresponse")
  add_lsdalton_test(NAME LSDALTON_UHF_excit             )
  add_lsdalton_test(NAME LSDALTON_UHF_excit2            )
  add_lsdalton_test(NAME LSDALTON_UHF_excit_radical     )
else()
  add_lsdalton_runtest_v1(
    NAME
      LSDALTON_df_admm_alpha_cam100_psfun
    )
endif()
