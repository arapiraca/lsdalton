#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_HF_cartesian.info <<'%EOF%'
   LSDALTON_HF_cartesian
   -------------
   Molecule:         Methane with -OH,and -Cl and -F
   Wave Function:    HF
   Test Purpose:     Check use of cartesian basis functions
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_HF_cartesian.mol <<'%EOF%'
ATOMBASIS
Methane with -OH,and -Cl and -F
b3lyp/6-31+g(d,p) optimized geo.
Atomtypes=5 Nosymmetry Cartesian
Charge=1. Atoms=2 Basis=6-31G
H    0.520821    0.080551    1.451675
H    1.965224   -1.097271    0.053015
Charge=6.    Atoms=1  Basis=6-31G
C    0.430423    0.005605    0.364687
Charge=8.    Atoms=1  Basis=3-21G
O    1.023374   -1.101579   -0.177807
Charge=9.    Atoms=1  Basis=3-21G
F    0.989971    1.157250   -0.160167
Charge=17.    Atoms=1  Basis=Turbomole-DZP
Cl   -1.303842   -0.036443   -0.048756
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_HF_cartesian.dal <<'%EOF%'
**GENERAL
.TESTMPICOPY
.NOGCBASIS
**INTEGRALS
.CART-E
**WAVE FUNCTIONS
.HF
*DENSOPT
.RH
.DIIS
.START
H1DIAG
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_HF_cartesian.check
cat >> LSDALTON_HF_cartesian.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi


CRIT1=`$GREP "Final HF energy:   * \-664\.4347167[1-3]" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
