#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > cartes_trilevel_newconv.info <<'%EOF%'
   dft_lda_molhes 
   -------------
   Molecule:         Water
   Wave Function:    DFT / LDA / 6-31G / Ahlrichs-Coulomb-Fit
   Test Purpose:     Test geometry optimizer in Cartesian coordinates
   with TRILEVEL starting guess and a default convergence criteria, which
   is set to .LOOSE. DFP Hessian update.
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > cartes_trilevel_newconv.mol <<'%EOF%'
BASIS
6-31G Aux=Ahlrichs-Coulomb-Fit
water R(OH) = 0.95Aa , <HOH = 109 deg.
Distance in Aangstroms
   2     0         A
        8.    1
O      0.00000   0.00000   0.00000
        1.    2
H      0.55168   0.77340   0.00000
H      0.55168  -0.77340   0.00000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > cartes_trilevel_newconv.dal <<'%EOF%'
**OPTIMI
.CARTES
.INITEV
1.D0
.DFP
.LOOSE
**WAVE FUNCTIONS
.DFT
LDA
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.START
 TRILEVEL
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > cartes_trilevel_newconv.check
cat >> cartes_trilevel_newconv.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-75\.81990075" $log | wc -l` 
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Number of energies
CRIT3=`$GREP "Final * DFT energy" $log | wc -l` 
TEST[3]=`expr   $CRIT3`
CTRL[3]=20
ERROR[3]="Wrong number of geometry steps"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
