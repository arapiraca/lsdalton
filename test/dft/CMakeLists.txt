add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d2_b3lyp     )
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d2_blyp      )
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d2_bp86      )
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3_Slater    )
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3_b3lyp     )
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3_camb3lyp  )
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3_olyp      )
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3_blyp      )
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3_bp86      )
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3bj_b3lyp   )
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3bj_camb3lyp)
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3bj_olyp    )
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3bj_blyp    )
add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3bj_bp86    )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_vwn3         )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_bp86         )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_bpw91        )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_pw86x        )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_lda          )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_svwn5        )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_svwn3        )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_b3lyp        )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_b3lyp-g      )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_b3p86        )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_b3p86-g      )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_blyp         )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_kt1          )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_kt2          )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_kt3          )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_olyp         )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_camb3lyp     )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_camb3lyp100  )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_becke        )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_slater       )
add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_psfun_lb94   )

# Conditionally available tests
#Special case for pbe and pbe0 due to a ~e-5 discreapancy between Pawel Salek's
#PSFUN code and XCFUN. XCFUN pbe version is correct, and PSFUN is only slightly
#wrong
if(ENABLE_XCFUN)
  add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d2_pbe   )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d2_pbe0  )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3_pbe   )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3_pbe0  )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3bj_pbe )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3bj_pbe0)
  add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_xcfun_pbe0          LABELS "ContinuousIntegration")
  add_lsdalton_runtest(NAME LSDALTON_dftfunc_xcfun_pbe           LABELS "ContinuousIntegration")
  add_lsdalton_runtest(NAME LSDALTON_dftfunc_xcfun_lda           LABELS "ContinuousIntegration")
  add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_b97-1                  )
else()
  add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d2_psfun_pbe           )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d2_psfun_pbe0          )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3_psfun_pbe           )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3_psfun_pbe0          )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3bj_psfun_pbe         )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftdisp_d3bj_psfun_pbe0        )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_psfun_pbe0             )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_psfun_pbe              )
  add_lsdalton_runtest_v1(NAME LSDALTON_dftfunc_b97-1_psfun            )
endif()
