#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_excit_restart.info <<'%EOF%'
   LSresponse_excit_restart 
   -------------
   Molecule:         CO
   Wave Function:    HF
   Test Purpose:     Test Restart option for excitation energies 
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_excit_restart.mol <<'%EOF%'
BASIS                                   
6-31G
==============
=============                           
Atomtypes=2 Nosymmetry Angstrom                    
Charge=6.0 Atoms=1
C   0.0000000   0.000000   0.000000  
Charge=8.0 Atoms=1
O   1.1280000   0.000000   0.000000   
%EOF%

#######################################################################
#  FIRST DALTON INPUT
#######################################################################
cat > LSresponse_excit_restart.dal <<'%EOF%'
**WAVE FUNCTIONS
.HF
*DENSOPT
.RH
.DIIS
.CONVTHR
1.d-7
.START
ATOMS
**RESPONSE
*LINRSP
.NEXCIT
2
*SOLVER
.CONVTHR
1.0D-5
*END OF INPUT
%EOF%

#######################################################################
#  SECOND DALTON INPUT
#######################################################################
cat > LSresponse_excit_restart.dal.2 <<'%EOF%'
**WAVE FUNCTIONS
.HF
*DENSOPT
.RESTART
.RH
.DIIS
.CONVTHR
1.d-7
.START
ATOMS
**RESPONSE
*LINRSP
.NEXCIT
4
*SOLVER
.RESTEXC
2
.CONVTHR
1.0D-5
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSresponse_excit_restart.check
cat >>LSresponse_excit_restart.check <<'%EOF%'

log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

#Use the ',molcfg%solver%rsp_eigenvecs,' excitation vectors from the rsp_eigenvecs file
# ENERGY                                         
CRIT1=`$GREP "RESTART FROM DENSITY ON DISK" $log | wc -l`  
CRIT2=`$GREP "Restart from * 2 vectors from the rsp\_eigenvecs file" $log | wc -l`       
TEST[1]=`expr   $CRIT1 + $CRIT2`                        
CTRL[1]=2                                                           
ERROR[1]="RESTART NOT CORRECT -"  

# ENERGY                                         
CRIT1=`$GREP "Final HF energy\: *\-112\.6672045" $log | wc -l`       
TEST[2]=`expr   $CRIT1`
CTRL[2]=1
ERROR[2]="HF ENERGY NOT CORRECT -"  

# excitation energies 
CRIT1=`$GREP "1 * 0\.311061" $log | wc -l` 
CRIT2=`$GREP "2 * 0\.311061" $log | wc -l` 
CRIT3=`$GREP "3 * 0\.335697" $log | wc -l` 
CRIT4=`$GREP "4 * 0\.361997" $log | wc -l` 
TEST[3]=`expr   $CRIT1 + $CRIT2 + $CRIT3 + $CRIT4`     
CTRL[3]=4   
ERROR[3]="Excitation energies not correct -"     

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=1
ERROR[4]="Memory leak -"

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
