#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_DFT_tpa.info <<'%EOF%'
   LSresponse_DFT_tpa
   -----------------
   Molecule:         HF
   Wave Function:    DFT-B3LYP / STO-3G
   Test Purpose:     Test excitation energy and TPA (Kasper K)
                    
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_DFT_tpa.mol <<'%EOF%'
BASIS
STO-3G
-----------test----------

Atomtypes=2 Generators=0
Charge=1.0 Atoms=1 Basis=STO-3G
H 0.000000000 0.0000000000   0.000000000000000
Charge=9.0 Atoms=1 Basis=STO-3G
F 1.96000000  0.0000000000   0.000000000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSresponse_DFT_tpa.dal <<'%EOF%'
**WAVE FUNCTIONS
.DFT
B3LYP
*DENSOPT
.ARH DAVID     
.CONVDYN
TIGHT
**RESPONSE
.NEXCIT 
3
*TPA
*END OF INPUT  
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > LSresponse_DFT_tpa.check
cat >> LSresponse_DFT_tpa.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-98\.886[7-8]" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Excitation energy and isotropically averaged TPA
CRIT1=`$GREP "3 * 0.744[0-9][0-9][0-9][0-9][0-9] * 1.282[0-9][0-9][0-9][0-9] * 0\.528" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Error in excitation energy / isotropically averaged TPA"

# tpa transition tensor components
CRIT1=`$GREP "Ex * \-?2.291" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=1
ERROR[4]="Error in TPA transition tensor components"


PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
