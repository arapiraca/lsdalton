 THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDALTON - An electronic structure program  **********    
     ******************************************************************    
  
  
      This is output from LSDALTON 2016.alpha
  
  
      IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
      THE FOLLOWING PAPER SHOULD BE CITED:
      
      K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
      L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
      P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
      J. J. Eriksen, P. Ettenhuber, B. Fernandez,
      L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
      A. Halkier, C. Haettig, H. Heiberg,
      T. Helgaker, A. C. Hennum, H. Hettema,
      E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
      M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
      D. Jonsson, P. Joergensen, J. Kauczor,
      S. Kirpekar, T. Kjaergaard, W. Klopper,
      S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
      A. Krapp, K. Kristensen, A. Ligabue,
      O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
      C. Neiss, C. B. Nielsen, P. Norman,
      J. Olsen, J. M. H. Olsen, A. Osted,
      M. J. Packer, F. Pawlowski, T. B. Pedersen,
      P. F. Provasi, S. Reine, Z. Rinkevicius,
      T. A. Ruden, K. Ruud, V. Rybkin,
      P. Salek, C. C. M. Samson, A. Sanchez de Meras,
      T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
      K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
      P. R. Taylor, A. M. Teale, E. I. Tellgren,
      D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
      O. Vahtras, M. A. Watson, D. J. D. Wilson,
      M. Ziolkowski, and H. AAgren,
      "The Dalton quantum chemistry program system",
      WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                                
      LSDALTON authors in alphabetical order (main contribution(s) in parenthesis)
      ----------------------------------------------------------------------------
      Vebjoern Bakken,         University of Oslo,        Norway    (geometry optimizer)
      Radovan Bast,            KTH Stockholm,             Sweden    (CMake, Testing)
      Sonia Coriani,           University of Trieste,     Italy     (response)
      Patrick Ettenhuber,      Aarhus University,         Denmark   (CCSD)
      Trygve Helgaker,         University of Oslo,        Norway    (supervision)
      Stinne Hoest,            Aarhus University,         Denmark   (SCF optimization)
      Ida-Marie Hoeyvik,       Aarhus University,         Denmark   (orbital localization, SCF optimization)
      Robert Izsak,            University of Oslo,        Norway    (ADMM)
      Branislav Jansik,        Aarhus University,         Denmark   (trilevel, orbital localization)
      Poul Joergensen,         Aarhus University,         Denmark   (supervision)
      Joanna Kauczor,          Aarhus University,         Denmark   (response solver)
      Thomas Kjaergaard,       Aarhus University,         Denmark   (response, integrals)
      Andreas Krapp,           University of Oslo,        Norway    (FMM, dispersion-corrected DFT)
      Kasper Kristensen,       Aarhus University,         Denmark   (response, DEC)
      Patrick Merlot,          University of Oslo,        Norway    (ADMM)
      Cecilie Nygaard,         Aarhus University,         Denmark   (SOEO)
      Jeppe Olsen,             Aarhus University,         Denmark   (supervision)
      Simen Reine,             University of Oslo,        Norway    (integrals, geometry optimizer)
      Vladimir Rybkin,         University of Oslo,        Norway    (geometry optimizer, dynamics)
      Pawel Salek,             KTH Stockholm,             Sweden    (FMM, DFT functionals)
      Andrew M. Teale,         University of Nottingham   England   (E-coefficients)
      Erik Tellgren,           University of Oslo,        Norway    (density fitting, E-coefficients)
      Andreas J. Thorvaldsen,  University of Tromsoe,     Norway    (response)
      Lea Thoegersen,          Aarhus University,         Denmark   (SCF optimization)
      Mark Watson,             University of Oslo,        Norway    (FMM)
      Marcin Ziolkowski,       Aarhus University,         Denmark   (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
 Who compiled             | tkjaer
 Host                     | localhost.localdomain
 System                   | Linux-3.16.6-200.fc20.x86_64
 CMake generator          | Unix Makefiles
 Processor                | x86_64
 64-bit integers          | OFF
 MPI                      | OFF
 Fortran compiler         | /opt/intel/composer_xe_2013_sp1.3.174/bin/intel64/
                          | ifort
 Fortran compiler version | ifort (IFORT) 14.0.3 20140422
 C compiler               | /opt/intel/composer_xe_2013_sp1.3.174/bin/intel64/
                          | icc
 C compiler version       | icc (ICC) 14.0.3 20140422
 C++ compiler             | /opt/intel/composer_xe_2013_sp1.3.174/bin/intel64/
                          | icpc
 C++ compiler version     | icpc (ICC) 14.0.3 20140422
 BLAS                     | /opt/intel/composer_xe_2013_sp1.3.174/mkl/lib/inte
                          | l64/libmkl_intel_lp64.so;/opt/intel/composer_xe_20
                          | 13_sp1.3.174/mkl/lib/intel64/libmkl_sequential.so;
                          | /opt/intel/composer_xe_2013_sp1.3.174/mkl/lib/inte
                          | l64/libmkl_core.so;/usr/lib64/libpthread.so;/usr/l
                          | ib64/libm.so
 LAPACK                   | /opt/intel/composer_xe_2013_sp1.3.174/mkl/lib/inte
                          | l64/libmkl_lapack95_lp64.a;/opt/intel/composer_xe_
                          | 2013_sp1.3.174/mkl/lib/intel64/libmkl_intel_lp64.s
                          | o
 Static linking           | OFF
 Last Git revision        | bb2dfa3833d600e06cd4888eae18f70a2873556d
 Git branch               | master
 Configuration time       | 2015-03-03 15:17:26.600659

Start simulation
     Date and time (Linux)  : Tue Mar  3 15:18:30 2015
     Host name              : localhost.localdomain                   
                      
 -----------------------------------------
          PRINTING THE MOLECULE.INP FILE 
 -----------------------------------------
                      
  BASIS                                   
  6-31G                                   
  water R(OH) = 0.95Aa , <HOH = 109 deg.  
  Distance in Aangstroms                  
  AtomTypes=2 Charge=0 Nosymmetry Angstrom                    
  Charge=8. Atoms=1                                           
  O      0.00000   0.00000   0.00000                          
  Charge=1. Atoms=2                                           
  H      0.55168   0.77340   0.00000                          
  H      0.55168  -0.77340   0.00000                          
                      
 -----------------------------------------
          PRINTING THE LSDALTON.INP FILE 
 -----------------------------------------
                      
  **GENERAL                                                                                           
  .TIME                                                                                               
  .NOGCBASIS                                                                                          
  **PROFILE                                                                                           
  .ICHORDEC                                                                                           
  **INTEGRALS                                                                                         
  .NOJENGINE                                                                                          
  **WAVE FUNCTIONS                                                                                    
  .HF                                                                                                 
  *END OF INPUT                                                                                       
  
  MOLPRINT IS SET TO       0
  Coordinates are entered in Angstroms and converted to atomic units.
  Conversion factor : 1 bohr = 0.52917721 A


                    Cartesian Coordinates Linsca (au)
                    ---------------------------------

   Total number of coordinates:  9
   Written in atomic units    
 
   1   O        x      0.0000000000
   2            y      0.0000000000
   3            z      0.0000000000
 
   4   H        x      1.0425241136
   5            y      1.4615141920
   6            z      0.0000000000
 
   7   H        x      1.0425241136
   8            y     -1.4615141920
   9            z      0.0000000000
 
 
 BASISSETLIBRARY  : REGULAR  
   
BASISSETLIBRARY
 Number of Basisset           1
 BASISSET:  6-31G                                             
  CHARGES:    8.0000    1.0000
                      
   
  CALLING BUILD BASIS WITH DEFAULT REGULAR   BASIS
   
 OPENING FILE
 /home/tkjaer/DaltonDevelopment2015/RIDensity/build-debug/basis/6-31G
 PRINT MOLECULE AND BASIS IN FULL INPUT
                      
THE MOLECULE
 --------------------------------------------------------------------
 Molecular Charge                    :    0.0000
    Regular basisfunctions             :       13
    Valence basisfunctions             :        0
    Primitive Regular basisfunctions   :       30
    Primitive Valence basisfunctions   :        0
 --------------------------------------------------------------------
                      
                      
 --------------------------------------------------------------------
  atom  charge  Atomicbasis    Phantom   nPrimREG  nContREG
 --------------------------------------------------------------------
     1   8.000  6-31G           F             22           9
     2   1.000  6-31G           F              4           2
     3   1.000  6-31G           F              4           2
                      
 The cartesian centers in Atomic units. 
  ATOM  NAME  ISOTOPE            X                 Y                 Z     
     1  O           1        0.00000000        0.00000000        0.00000000
     2  H           1        1.04252411        1.46151419        0.00000000
     3  H           1        1.04252411       -1.46151419        0.00000000
                      
                      
Atoms and basis sets
  Total number of atoms        :      3
  THE  REGULAR   is on R =   1
---------------------------------------------------------------------
  atom label  charge basisset                prim     cont   basis
---------------------------------------------------------------------
      1 O      8.000 6-31G                     22        9 [10s4p|3s2p]                                 
      2 H      1.000 6-31G                      4        2 [4s|2s]                                      
      3 H      1.000 6-31G                      4        2 [4s|2s]                                      
---------------------------------------------------------------------
total         10                               30       13
---------------------------------------------------------------------
                      
 Configuration:
 ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)
 
   You have requested Augmented Roothaan-Hall optimization
   => explicit averaging is turned off!
 
Expand trust radius if ratio is larger than:            0.75
Contract trust radius if ratio is smaller than:         0.25
On expansion, trust radius is expanded by a factor      1.20
On contraction, trust radius is contracted by a factor  0.70
 
 Maximum size of subspace in ARH linear equations:           2
 
Density subspace min. method    : None                    
Density optimization : Augmented RH optimization          
 
 Maximum size of Fock/density queue in averaging:          10
 
Convergence threshold for gradient:   0.10E-03
We perform the calculation in the standard input basis
 
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
 The SCF Convergence Criteria is applied to the gradnorm in OAO basis
 
 End of configuration!
 
 >>>  CPU Time used in *INPUT is   0.01 seconds
 >>> wall Time used in *INPUT is   0.01 seconds
 =============================================
  
  Profile Subroutine:
  Number of threads:  1
  
  Ichor Integrals: F
 =============================================
 >>>  CPU Time used in OVERLAP is   0.00 seconds
 >>> wall Time used in OVERLAP is   0.00 seconds
 QQQ S:   24.4924871775310     
 >>>  CPU Time used in CS15_INPUT-Mole is   0.03 seconds
 >>> wall Time used in CS15_INPUT-Mole is   0.03 seconds
 >>>  CPU Time used in CS15_INPUT-Mole is   0.00 seconds
 >>> wall Time used in CS15_INPUT-Mole is   0.00 seconds
 >>>  CPU Time used in NucElec is   0.04 seconds
 >>> wall Time used in NucElec is   0.04 seconds
 >>>  CPU Time used in Kinetic is   0.00 seconds
 >>> wall Time used in Kinetic is   0.00 seconds
 QQQ h:   2356.53258884503     
 
 First density: Atoms in molecule guess
 
 >>>  CPU Time used in reg-J is   0.06 seconds
 >>> wall Time used in reg-J is   0.06 seconds
 >>>  CPU Time used in reg-Jbuild is   0.06 seconds
 >>> wall Time used in reg-Jbuild is   0.06 seconds
 The Coulomb energy contribution    55.2691076147586     
 >>>  CPU Time used in LINK-Kbuild is   0.03 seconds
 >>> wall Time used in LINK-Kbuild is   0.03 seconds
 The Exchange energy contribution    9.72760304012899     
 The Fock energy contribution   -45.5415045746296     
  Iteration 0 energy:  -75.2585856196225     
 QQQ D:   2.95114685860154     
 Succesfully wrote valid Density Matrix to Disk
 >>>  CPU Time used in SCREENDECJ is   0.01 seconds
 >>> wall Time used in SCREENDECJ is   0.01 seconds
 MinimumAllowedAObatchSize           3
 MinimumAllowedAObatchSize           3
 RequestedOrbitalDimOfAObatch           3
 nbatchesofAOS           5
 >>>  CPU Time used in DECJ is   0.11 seconds
 >>> wall Time used in DECJ is   0.11 seconds
 >>>  CPU Time used in reg-J is   0.06 seconds
 >>> wall Time used in reg-J is   0.06 seconds
 >>>  CPU Time used in reg-Jbuild is   0.06 seconds
 >>> wall Time used in reg-Jbuild is   0.06 seconds
 QQQ DI_DEBUG_DECPACK J STD       1211.65071785693     
 QQQ DI_DEBUG_DECPACK J DECPACK   1211.65071785693     
 QQQ DIFF  1.694456258367382E-028
 QQQ SUCCESSFUL DECPACK J TEST
 >>>  CPU Time used in CS15_INPUT-Mole is   0.00 seconds
 >>> wall Time used in CS15_INPUT-Mole is   0.00 seconds
 >>>  CPU Time used in reg-J is   0.10 seconds
 >>> wall Time used in reg-J is   0.10 seconds
 >>>  CPU Time used in reg-Jbuild is   0.10 seconds
 >>> wall Time used in reg-Jbuild is   0.10 seconds
 >>>  CPU Time used in DECJOLD is   0.22 seconds
 >>> wall Time used in DECJOLD is   0.22 seconds
 QQQ OLD DI_DEBUG_DECPACK J STD       1211.65071785693     
 QQQ OLD DI_DEBUG_DECPACK J DECPACK   1211.65071785693     
 QQQ OLD DIFF  1.430585589581509E-026
 QQQ SUCCESSFUL DECPACK JOLD TEST
 >>>  CPU Time used in CS15_INPUT-Mole is   0.00 seconds
 >>> wall Time used in CS15_INPUT-Mole is   0.00 seconds
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Memory statistics          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (TOTAL):                136175 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(matrix)):           1352 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (real):                  13968 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MPI):                       0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (complex):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integer):                1383 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (logical):                 640 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (character):              1600 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (AOBATCH):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODBATCH):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSAOTENSOR):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (SLSAOTENSOR):            3072 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (GLOBALLSAOTENSOR):          0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMTYPEITEM):         112528 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMITEM):               1632 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSMATRIX):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECORBITAL):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECFRAG):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlapType):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (BATCHTOORB):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECAOBATCHINFO):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MYPOINTER):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (fragmentAOS):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY2):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY4):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY):                     0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (PNOSpaceInfo):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2DENS):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (TRACEBACK):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2GRAD):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(lvec_data)):           0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(lattice_cell)):        0 byte  - Should be zero - otherwise a leakage is present
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Additional Memory information          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (linkshell):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integrand):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integralitem):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (intwork):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlap):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODitem):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (lstensor):              935 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (FMM   ):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (XC    ):                  0 byte  - Should be zero - otherwise a leakage is present
  Max allocated memory, TOTAL                         85.062 MB
  Max allocated memory, type(matrix)                  11.368 kB
  Max allocated memory, real(realk)                  960.160 kB
  Max allocated memory, MPI                            0.000 Byte
  Max allocated memory, complex(complexk)              0.000 Byte
  Max allocated memory, integer                       21.915 kB
  Max allocated memory, logical                      200.768 kB
  Max allocated memory, character                      2.320 kB
  Max allocated memory, AOBATCH                       29.232 kB
  Max allocated memory, DECORBITAL                     0.000 Byte
  Max allocated memory, DECFRAG                        0.000 Byte
  Max allocated memory, BATCHTOORB                     0.400 kB
  Max allocated memory, DECAOBATCHINFO                 0.100 kB
  Max allocated memory, MYPOINTER                      0.000 Byte
  Max allocated memory, fragmentAOS                    0.000 Byte
  Max allocated memory, ARRAY2                         0.000 Byte
  Max allocated memory, ARRAY4                         0.000 Byte
  Max allocated memory, ARRAY                         84.400 MB
  Max allocated memory, PNOSpaceInfo                   0.000 Byte
  Max allocated memory, MP2DENS                        0.000 Byte
  Max allocated memory, TRACEBACK                      0.000 Byte
  Max allocated memory, MP2GRAD                        0.000 Byte
  Max allocated memory, ODBATCH                        4.752 kB
  Max allocated memory, LSAOTENSOR                     7.632 kB
  Max allocated memory, SLSAOTENSOR                   16.896 kB
  Max allocated memory, GLOBALLSAOTENSOR               0.000 Byte
  Max allocated memory, ATOMTYPEITEM                 112.528 kB
  Max allocated memory, ATOMITEM                       1.632 kB
  Max allocated memory, LSMATRIX                       7.040 kB
  Max allocated memory, OverlapT                     141.120 kB
  Max allocated memory, linkshell                      0.924 kB
  Max allocated memory, integrand                     72.576 kB
  Max allocated memory, integralitem                 829.440 kB
  Max allocated memory, IntWork                       28.224 kB
  Max allocated memory, Overlap                       23.212 kB
  Max allocated memory, ODitem                         3.456 kB
  Max allocated memory, LStensor                      13.235 kB
  Max allocated memory, FMM                            0.000 Byte
  Max allocated memory, XC                             0.000 Byte
  Max allocated memory, Lvec_data                      0.000 Byte
  Max allocated memory, Lattice_cell                   0.000 Byte
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
 
  Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
  This is a non MPI calculation so naturally no memory is allocated on slaves!
      ===============================================================
      Overall Timings for the Matrix Operations and Integral routines
               in Level 3 The Full molecular Calculation             
      Matrix Operation   # calls          CPU Time         Wall Time
      ===============================================================
   L3 mat_set_from_full        3            0.0000            0.0000
   L3 mat_to_full              2            0.0000            0.0000
   L3 mat_trans                6            0.0000            0.0000
   L3 mat_assign               4            0.0000            0.0000
   L3 mat_trAB                11            0.0000            0.0000
   L3 mat_add                  8            0.0000            0.0000
   L3 mat_daxpy                4            0.0000            0.0000
   L3 mat_dotproduct           5            0.0000            0.0000
   L3 mat_sqnorm2              8            0.0000            0.0000
   L3 mat_diag_f               1            0.0000            0.0010
   L3 mat_scal                 3            0.0000            0.0000
   L3 mat_zero                 5            0.0000            0.0000
   L3 mat_density_from_o       1            0.0000            0.0000
   L3 mat_write_to_disk        1            0.0000            0.0000
      ==============================================================
   L3 TOTAL MAT                             0.0000            0.0010
      ==============================================================
   L3     II_get_overlap       1            0.0030            0.0030
   L3     II_get_kinetic       1            0.0020            0.0020
   L3   II_get_nucel_mat       1            0.0430            0.0430
   L3 II_precalc_ScreenM       2            0.0710            0.0710
   L3      II_get_nucpot       1            0.0000            0.0000
   L3 II_GET_EXCHANGE_MA       1            0.0300            0.0300
   L3 II_get_coulomb_mat       3            0.2200            0.2210
   L3 II_GET_DECPACKED4C      15            0.2210            0.2240
      ==============================================================
   L3 TOTAL INTEGRAL                        0.6470            0.6520
      ==============================================================
 >>>  CPU Time used in LSDALTON is   0.81 seconds
 >>> wall Time used in LSDALTON is   0.81 seconds

End simulation
     Date and time (Linux)  : Tue Mar  3 15:18:30 2015
     Host name              : localhost.localdomain                   
