bname=$(basename "$0")
bname=${bname/.bash/}
##########
export PATH=/usr/local/sbin:/bin:/usr/bin:/sbin
source /opt/intel/compilers_and_libraries_2017.1.132/linux/bin/compilervars.sh intel64
source /opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/bin/mklvars.sh intel64
source /opt/intel/compilers_and_libraries_2017.1.132/linux/mpi/intel64/bin/mpivars.sh

export MATH_ROOT=$MKLROOT
export CTEST_PROJECT_NAME=LSDALTON
#export DALTON_TMPDIR=SCRATCHDIRINTELMPI
export OMP_NUM_THREADS=2
export DALTON_NUM_MPI_PROCS=3
export CTEST_MAKE_NUM_PROCS=3
export LSDALTON_DEVELOPER=1
#
wrk=$1
lib=lsdalton_$bname
export DALTON_TMPDIR=$wrk/$lib/$bname/tmp
#
if [ ! -d $wrk/$lib ]
then
   cd $wrk
   git clone --recursive git@gitlab.com:dalton/lsdalton.git $lib
fi
#
cd $wrk/$lib
#
if [ ! -d $wrk/$lib/$bname ]
then

    ./setup --mpi --omp -DENABLE_DEC=ON -DENABLE_TENSORS=ON -DUSE_MPIF_H=ON -DBUILDNAME="$bname" $bname
fi
if [ ! -d $DALTON_TMPDIR ]
then
   mkdir $DALTON_TMPDIR
fi
#
cd $bname
#
ctest -D Nightly -L linsca
