list(APPEND LSDALTON_CXX_FLAGS
  "-fno-rtti"
  "-fno-exceptions"
  )

if(CMAKE_HOST_SYSTEM_PROCESSOR MATCHES "i386")
  list(APPEND LSDALTON_CXX_FLAGS
    "-m32"
    )
elseif(CMAKE_HOST_SYSTEM_PROCESSOR MATCHES "x86_64")
  list(APPEND LSDALTON_CXX_FLAGS
    "-m64"
    )
endif()

if(ENABLE_STATIC_LINKING)
  list(APPEND LSDALTON_CXX_FLAGS
    "-static"
    )
endif()

list(APPEND LSDALTON_CXX_FLAGS_DEBUG
  "-Og"
  "-g3"
  "-Wall"
  )

list(APPEND LSDALTON_CXX_FLAGS_RELEASE
  "-O3"
  "-ffast-math"
  "-funroll-loops"
  "-ftree-vectorize"
  "-Wno-unused"
  )
