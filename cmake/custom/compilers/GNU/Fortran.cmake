list(APPEND LSDALTON_Fortran_FLAGS
  "-ffloat-store"
  "-fcray-pointer"
  "-fautomatic"
  "-fmax-errors=5"
  )

if(CMAKE_HOST_SYSTEM_PROCESSOR MATCHES "i386")
  list(APPEND LSDALTON_Fortran_FLAGS
    "-m32"
    )
elseif(CMAKE_HOST_SYSTEM_PROCESSOR MATCHES "x86_64")
  list(APPEND LSDALTON_Fortran_FLAGS
    "-m64"
    )
endif()

if(ENABLE_STATIC_LINKING)
  list(APPEND LSDALTON_Fortran_FLAGS
    "-static"
    )
endif()

list(APPEND LSDALTON_Fortran_FLAGS_DEBUG
  "-Og"
  "-g3"
  "-fbacktrace"
  "-fcray-pointer"
  "-Wuninitialized"
  "-Waliasing"
  "-Wampersand"
  "-Wcharacter-truncation"
  "-Wline-truncation"
  "-Wsurprising"
  "-Wunderflow"
  )

list(APPEND LSDALTON_Fortran_FLAGS_RELEASE
  "-O3"
  "-ffast-math"
  "-funroll-loops"
  "-ftree-vectorize"
  )

list(APPEND LSDALTON_Fortran_FLAGS_COVERAGE
  "${CODE_COVERAGE_FLAGS}"
  )
