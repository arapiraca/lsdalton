#.rst:
#
# Variables modified (provided the corresponding language is enabled)::
#
#   LSDALTON_Fortran_FLAGS
#
# autocmake.yml configuration::
#
#   docopt:
#     - "--check Enable bounds checking [default: False]."
#     - "--int64 Enable 64bit integers [default: False]."
#     - "--static Enable static linking [default: False]."
#   define: "'-DENABLE_BOUNDS_CHECK={0} -DENABLE_64BIT_INTEGERS={0} -DENABLE_STATIC_LINKING={0}'.format(arguments['--check'], arguments['--int64'], arguments['--static'])"

option(ENABLE_BOUNDS_CHECK "Enable bounds checking" OFF)
option(ENABLE_STATIC_LINKING "Enable static linking" OFF)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)

include(${CMAKE_CURRENT_LIST_DIR}/compilers/FortranFlags.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/compilers/CFlags.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/compilers/CXXFlags.cmake)
