[![pipeline status](https://gitlab.com/dalton/lsdalton/badges/master/pipeline.svg)](https://gitlab.com/dalton/lsdalton/pipelines)

Nightly runs on full testset: https://testboard.org/cdash/index.php?project=LSDalton

## Quick start

### Getting the source code

Clone the repository:
```
$ git clone https://gitlab.com/dalton/lsdalton.git
```

This will fetch the entire repository in a directory called *lsdalton*. By default
it checks out the master branch which is the main development branch. To
checkout a specific release version, run the following command from inside the
*lsdalton* directory:
```
$ git checkout v2018.0
```
replacing *v2018.0* by the release version that you are interesed in. Note
that it is currently not possible to fetch all past releases.

### Satisfying build dependencies

At the very least, building LSDALTON requires:

* Fortran, C, and C++ compilers.
* the [CMake](https://cmake.org) build system generator.

The minimum version of CMake required is rather recent (3.14) and on many Linux distributions is not available as a system package.
On Linux, you can run the following commands to install CMake 3.14.7:
```
CMAKE_VERSION=3.14.7
target_path=$HOME/Deps/cmake/$CMAKE_VERSION
cmake_url="https://cmake.org/files/v${CMAKE_VERSION%.*}/cmake-${CMAKE_VERSION}-Linux-x86_64.tar.gz"
mkdir -p "$target_path"
curl -Ls "$cmake_url" | tar -xz -C "$target_path" --strip-components=1
export PATH=$HOME/Deps/cmake/$CMAKE_VERSION/bin${PATH:+:$PATH}
```

Alternatively, with
[Anaconda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)
installed you can create a [Conda environment](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html)
from the `environment.yml` file shipped alongside the code:
```
conda create --name lsdalton python=3.7 --yes
conda env update --name lsdalton --file environment.yml
```
The environment thus created will include all dependencies.

Some submodules might have additional dependencies and you should thus check
their installation instructions.

### Building the code

To build the code:
```
$ ./setup [--help]
$ cd build
$ make [-j4]
```

Run the test set:
```
$ ctest [-j4]
```

## How to contribute

See Dalton Developer’s Guide: http://dalton-devguide.readthedocs.io

## Dalton links

- [Home page](http://daltonprogram.org/)
- [Forum](http://forum.daltonprogram.org/)
- [Article](http://onlinelibrary.wiley.com/doi/10.1002/wcms.1172/abstract)
